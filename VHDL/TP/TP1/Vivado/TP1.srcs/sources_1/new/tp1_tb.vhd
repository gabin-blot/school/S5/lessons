--------------------------------------------
-- TP1 Component Testbench

library ieee;
use ieee.std_logic_1164.all;

entity TP1_Testbench is
end TP1_Testbench;

architecture v1 of TP1_Testbench is

    Signal swt_tb : STD_LOGIC_VECTOR (7 downto 0);
    Signal led_tb : STD_LOGIC_VECTOR (7 downto 0);

    component TP1
        Port(
            swt : in STD_LOGIC_VECTOR (7 downto 0);
            led : out STD_LOGIC_VECTOR (7 downto 0)
        );
    end component;
    
      begin
        
        uut: TP1 Port map (
             swt=>swt_tb,
             led=>led_tb
         );
         
        stimulate: process
          begin
            swt_tb <= "00000000";            wait for 30 ns;
            swt_tb <= "00000001";            wait for 30 ns;
            
            swt_tb <= "00000010";            wait for 30 ns;
            swt_tb <= "00000100";            wait for 30 ns;
            swt_tb <= "00000110";            wait for 30 ns;
            swt_tb <= "00001000";            wait for 30 ns;
            swt_tb <= "00001010";            wait for 30 ns;
            swt_tb <= "00001100";            wait for 30 ns;
            swt_tb <= "00001110";            wait for 30 ns;
            
            
            swt_tb <= "00010000";            wait for 30 ns;
            swt_tb <= "00100000";            wait for 30 ns;
            swt_tb <= "01000000";            wait for 30 ns;
            swt_tb <= "10000000";            wait for 30 ns;
            
            wait for 300 ns;
        end process;
end v1;