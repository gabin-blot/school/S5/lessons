--------------------------------------------
-- TP1 Component

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity TP1 is
    Port ( 
        swt : in STD_LOGIC_VECTOR (7 downto 0);
        led : out STD_LOGIC_VECTOR (7 downto 0)
    );
end TP1;

architecture v1 of TP1 is

begin

    led(0) <= not  swt(0);
    led(1) <= swt(1) AND (NOT swt(2));
    led(2) <= ( swt(1) AND (NOT swt(2)) ) OR ( swt(2) AND swt(3) );
    led(3) <= swt(2) AND swt(3);
    
    boucle:for i in 4 to 7 generate
        led(i) <= swt(i);
    end generate;
    


end v1;
