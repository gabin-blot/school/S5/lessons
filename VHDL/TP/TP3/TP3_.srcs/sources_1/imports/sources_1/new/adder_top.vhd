----------------------------------------------------------------------------------
-- TP3
library ieee;
use ieee.std_logic_1164.all;

entity adder_top is
    port(
        sw: in std_logic_vector( 7 downto 0 );
        
        led : out STD_LOGIC_VECTOR ( 15 downto 0);
        
        seg: out std_logic_vector ( 0 to 6 );
        an: out std_logic_vector ( 3 downto 0);
        dp: out std_logic
    );
end adder_top;

architecture v1 of adder_top is

    Signal result: std_logic_vector ( 3 downto 0);
    
    component hex7seg
      port(
        entree: in std_logic_vector(3 downto 0);
        a_to_g: out std_logic_vector(6 downto 0));
    end component;
    
    component adder
        port(
            input: in std_logic_vector ( 7 downto 0 );
            s: out std_logic_vector ( 3 downto 0 );
            cf, ovf: out std_logic
        );
    end component;
    
      begin
      
        an<= "1110"; -- all digits on
        dp <= '1'; --dp off
        
        boucle:for i in 0 to 7 generate
            led(i) <= sw(i);
        end generate;
        
        boucle2:for i in 8 to 13 generate
            led(i) <= '0';
        end generate;
        
     
        computeResult: adder port map (
            input => sw,
            s => result,
            cf => led(15),
            ovf => led(14)
        );
            
        computeSeg: hex7seg port map (
            entree => result, 
            a_to_g => seg
        );
        
    
    
end v1;
