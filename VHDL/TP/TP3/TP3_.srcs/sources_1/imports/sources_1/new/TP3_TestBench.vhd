--------------------------------------------
-- TP3 7 Seg and adder Testbench

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity TP3_Testbench is
end TP3_Testbench;

architecture v1 of TP3_Testbench is

    Signal tb_sw: std_logic_vector( 7 downto 0 );
    
    Signal tb_led : STD_LOGIC_VECTOR ( 15 downto 0 );
    
    
    Signal tb_seg: std_logic_vector ( 0 to 6 );
    Signal tb_an:  std_logic_vector ( 3 downto 0 );
    Signal tb_dp: std_logic;

    component adder_top
        port(
            sw: in std_logic_vector( 7 downto 0 );
            
            led : out STD_LOGIC_VECTOR ( 15 downto 0);
                
            seg: out std_logic_vector ( 0 to 6 );
            an: out std_logic_vector ( 3 downto 0);
            dp: out std_logic
        );
    end component;

      begin

        uut: adder_top Port map (
          sw=>tb_sw,
          
          seg=>tb_seg,
          
          led=>tb_led,
          
          an=>tb_an,
          dp=>tb_dp
        );

        stimulate: process
          begin
    
            tb_sw <= (others => '0');
            wait for 50 ns;
            loop
                tb_sw <= tb_sw+1;
                wait for 50 ns;
            end loop;
        end process;
end v1;
