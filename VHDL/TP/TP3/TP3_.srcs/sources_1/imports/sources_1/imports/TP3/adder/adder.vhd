-----------------------------------
--Component for question 3

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity adder is
    port( 
        input: in std_logic_vector ( 7 downto 0 );
        s: out std_logic_vector ( 3 downto 0 );
        cf, ovf: out std_logic
    );
end adder;

architecture v1 of adder is
 begin
    process(input)
        variable temp: std_logic_vector(4 downto 0);
          begin
            temp := ( '0' & input(7 downto 4) ) + ( '0' & input(3 downto 0) ) ;
            s <= temp (3 downto 0) ;
            cf <= temp(4) ;
            ovf <= temp(3) xor input(7 downto 4)(7) xor input(3 downto 0)(3) xor temp(4) ;
    end process;
end v1;
-----------------------------------

