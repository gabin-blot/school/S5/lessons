-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Wed Feb 12 17:10:09 2020
-- Host        : gabin-VivoBook-S14-X430UF running 64-bit Ubuntu 19.10
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               /home/gabin/Documents/Cours/VHDL/TP/TP3/TP3_.sim/sim_1/impl/func/xsim/TP3_Testbench_func_impl.vhd
-- Design      : adder_top
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity adder_top is
  port (
    sw : in STD_LOGIC_VECTOR ( 7 downto 0 );
    led : out STD_LOGIC_VECTOR ( 15 downto 0 );
    seg : out STD_LOGIC_VECTOR ( 0 to 6 );
    an : out STD_LOGIC_VECTOR ( 3 downto 0 );
    dp : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of adder_top : entity is true;
  attribute ECO_CHECKSUM : string;
  attribute ECO_CHECKSUM of adder_top : entity is "16c32b57";
end adder_top;

architecture STRUCTURE of adder_top is
  signal led_OBUF : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \led_OBUF[15]_inst_i_2_n_0\ : STD_LOGIC;
  signal seg_OBUF : STD_LOGIC_VECTOR ( 0 to 6 );
  signal \seg_OBUF[0]_inst_i_2_n_0\ : STD_LOGIC;
  signal \seg_OBUF[0]_inst_i_3_n_0\ : STD_LOGIC;
  signal \seg_OBUF[0]_inst_i_4_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \led_OBUF[14]_inst_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \led_OBUF[15]_inst_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \seg_OBUF[0]_inst_i_3\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \seg_OBUF[0]_inst_i_4\ : label is "soft_lutpair0";
begin
\an_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => an(0)
    );
\an_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => an(1)
    );
\an_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => an(2)
    );
\an_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => an(3)
    );
dp_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => dp
    );
\led_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => led_OBUF(0),
      O => led(0)
    );
\led_OBUF[10]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => led(10)
    );
\led_OBUF[11]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => led(11)
    );
\led_OBUF[12]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => led(12)
    );
\led_OBUF[13]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => led(13)
    );
\led_OBUF[14]_inst\: unisim.vcomponents.OBUF
     port map (
      I => led_OBUF(14),
      O => led(14)
    );
\led_OBUF[14]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"42"
    )
        port map (
      I0 => \led_OBUF[15]_inst_i_2_n_0\,
      I1 => led_OBUF(3),
      I2 => led_OBUF(7),
      O => led_OBUF(14)
    );
\led_OBUF[15]_inst\: unisim.vcomponents.OBUF
     port map (
      I => led_OBUF(15),
      O => led(15)
    );
\led_OBUF[15]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => led_OBUF(3),
      I1 => led_OBUF(7),
      I2 => \led_OBUF[15]_inst_i_2_n_0\,
      O => led_OBUF(15)
    );
\led_OBUF[15]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEE888E8888888"
    )
        port map (
      I0 => led_OBUF(2),
      I1 => led_OBUF(6),
      I2 => led_OBUF(0),
      I3 => led_OBUF(4),
      I4 => led_OBUF(5),
      I5 => led_OBUF(1),
      O => \led_OBUF[15]_inst_i_2_n_0\
    );
\led_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => led_OBUF(1),
      O => led(1)
    );
\led_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => led_OBUF(2),
      O => led(2)
    );
\led_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => led_OBUF(3),
      O => led(3)
    );
\led_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => led_OBUF(4),
      O => led(4)
    );
\led_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => led_OBUF(5),
      O => led(5)
    );
\led_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => led_OBUF(6),
      O => led(6)
    );
\led_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => led_OBUF(7),
      O => led(7)
    );
\led_OBUF[8]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => led(8)
    );
\led_OBUF[9]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => led(9)
    );
\seg_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => seg_OBUF(0),
      O => seg(0)
    );
\seg_OBUF[0]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0096000096696900"
    )
        port map (
      I0 => \led_OBUF[15]_inst_i_2_n_0\,
      I1 => led_OBUF(3),
      I2 => led_OBUF(7),
      I3 => \seg_OBUF[0]_inst_i_2_n_0\,
      I4 => \seg_OBUF[0]_inst_i_3_n_0\,
      I5 => \seg_OBUF[0]_inst_i_4_n_0\,
      O => seg_OBUF(0)
    );
\seg_OBUF[0]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F880077F077FF880"
    )
        port map (
      I0 => led_OBUF(0),
      I1 => led_OBUF(4),
      I2 => led_OBUF(5),
      I3 => led_OBUF(1),
      I4 => led_OBUF(2),
      I5 => led_OBUF(6),
      O => \seg_OBUF[0]_inst_i_2_n_0\
    );
\seg_OBUF[0]_inst_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => led_OBUF(4),
      I1 => led_OBUF(0),
      O => \seg_OBUF[0]_inst_i_3_n_0\
    );
\seg_OBUF[0]_inst_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => led_OBUF(4),
      I1 => led_OBUF(0),
      I2 => led_OBUF(1),
      I3 => led_OBUF(5),
      O => \seg_OBUF[0]_inst_i_4_n_0\
    );
\seg_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => seg_OBUF(1),
      O => seg(1)
    );
\seg_OBUF[1]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9696FF0069009600"
    )
        port map (
      I0 => \led_OBUF[15]_inst_i_2_n_0\,
      I1 => led_OBUF(3),
      I2 => led_OBUF(7),
      I3 => \seg_OBUF[0]_inst_i_2_n_0\,
      I4 => \seg_OBUF[0]_inst_i_3_n_0\,
      I5 => \seg_OBUF[0]_inst_i_4_n_0\,
      O => seg_OBUF(1)
    );
\seg_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => seg_OBUF(2),
      O => seg(2)
    );
\seg_OBUF[2]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9696009600690000"
    )
        port map (
      I0 => \led_OBUF[15]_inst_i_2_n_0\,
      I1 => led_OBUF(3),
      I2 => led_OBUF(7),
      I3 => \seg_OBUF[0]_inst_i_3_n_0\,
      I4 => \seg_OBUF[0]_inst_i_4_n_0\,
      I5 => \seg_OBUF[0]_inst_i_2_n_0\,
      O => seg_OBUF(2)
    );
\seg_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => seg_OBUF(3),
      O => seg(3)
    );
\seg_OBUF[3]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00009600696900"
    )
        port map (
      I0 => \led_OBUF[15]_inst_i_2_n_0\,
      I1 => led_OBUF(3),
      I2 => led_OBUF(7),
      I3 => \seg_OBUF[0]_inst_i_2_n_0\,
      I4 => \seg_OBUF[0]_inst_i_3_n_0\,
      I5 => \seg_OBUF[0]_inst_i_4_n_0\,
      O => seg_OBUF(3)
    );
\seg_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => seg_OBUF(4),
      O => seg(4)
    );
\seg_OBUF[4]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"696969FF00690000"
    )
        port map (
      I0 => \led_OBUF[15]_inst_i_2_n_0\,
      I1 => led_OBUF(3),
      I2 => led_OBUF(7),
      I3 => \seg_OBUF[0]_inst_i_4_n_0\,
      I4 => \seg_OBUF[0]_inst_i_2_n_0\,
      I5 => \seg_OBUF[0]_inst_i_3_n_0\,
      O => seg_OBUF(4)
    );
\seg_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => seg_OBUF(5),
      O => seg(5)
    );
\seg_OBUF[5]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0096000069696900"
    )
        port map (
      I0 => \led_OBUF[15]_inst_i_2_n_0\,
      I1 => led_OBUF(3),
      I2 => led_OBUF(7),
      I3 => \seg_OBUF[0]_inst_i_4_n_0\,
      I4 => \seg_OBUF[0]_inst_i_3_n_0\,
      I5 => \seg_OBUF[0]_inst_i_2_n_0\,
      O => seg_OBUF(5)
    );
\seg_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => seg_OBUF(6),
      O => seg(6)
    );
\seg_OBUF[6]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6900000000966969"
    )
        port map (
      I0 => \led_OBUF[15]_inst_i_2_n_0\,
      I1 => led_OBUF(3),
      I2 => led_OBUF(7),
      I3 => \seg_OBUF[0]_inst_i_3_n_0\,
      I4 => \seg_OBUF[0]_inst_i_2_n_0\,
      I5 => \seg_OBUF[0]_inst_i_4_n_0\,
      O => seg_OBUF(6)
    );
\sw_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => sw(0),
      O => led_OBUF(0)
    );
\sw_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => sw(1),
      O => led_OBUF(1)
    );
\sw_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => sw(2),
      O => led_OBUF(2)
    );
\sw_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => sw(3),
      O => led_OBUF(3)
    );
\sw_IBUF[4]_inst\: unisim.vcomponents.IBUF
     port map (
      I => sw(4),
      O => led_OBUF(4)
    );
\sw_IBUF[5]_inst\: unisim.vcomponents.IBUF
     port map (
      I => sw(5),
      O => led_OBUF(5)
    );
\sw_IBUF[6]_inst\: unisim.vcomponents.IBUF
     port map (
      I => sw(6),
      O => led_OBUF(6)
    );
\sw_IBUF[7]_inst\: unisim.vcomponents.IBUF
     port map (
      I => sw(7),
      O => led_OBUF(7)
    );
end STRUCTURE;
