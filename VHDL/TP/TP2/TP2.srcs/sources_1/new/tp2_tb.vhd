--------------------------------------------
-- TP2 7 Seg Testbench

library ieee;
use ieee.std_logic_1164.all;

entity TP2_Testbench is
end TP2_Testbench;

architecture v1 of TP2_Testbench is

    Signal sw_tb: std_logic_vector( 3 downto 0 );
    Signal seg_tb: std_logic_vector ( 0 to 6 );
    Signal an_tb: std_logic_vector ( 3 downto 0);
    Signal dp_tb:  std_logic;

    component hex7seg_top
        Port(
            sw: in std_logic_vector( 3 downto 0 );
            seg: out std_logic_vector ( 0 to 6 );
            an: out std_logic_vector ( 3 downto 0);
            dp: out std_logic
        );
    end component;
    
      begin
        
        uut: hex7seg_top Port map (
            sw=>sw_tb,
            seg=>seg_tb,
            an=>an_tb,
            dp=>dp_tb
         );
         
        stimulate: process
          begin
            sw_tb <= x"0";      wait for 30 ns;
            sw_tb <= x"1";      wait for 30 ns;
            sw_tb <= x"2";      wait for 30 ns;
            sw_tb <= x"3";      wait for 30 ns;

        end process;
end v1;