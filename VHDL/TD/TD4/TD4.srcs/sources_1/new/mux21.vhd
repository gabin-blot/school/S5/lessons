library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity mux21a is
port(
    a: in std_logic;
    b: in std_logic;
    s: in std_logic;
    y: out std_logic
);
end mux21a;


architecture v1 of mux21a is
  begin
    y <= (not s and a) or (s and b) ;
end v1;