library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity mux21c is
port(
a: in std_logic;
b: in std_logic;
s: in std_logic;
y: out std_logic
);
end mux21c;

architecture v1 of mux21c is
    begin
    y <= a when s='0' else b;
end v1;