------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity mux21c_Testbench is
end mux21c_Testbench;

architecture v1 of mux21c_Testbench is
    
    signal y_tb: std_logic;
    signal input_tb: std_logic_vector(2 downto 0);
    
    component mux21c
    port(
        a: in std_logic;
        b: in std_logic;
        s: in std_logic;
        y: out std_logic
    );
    end component;
    
  begin
  
    uut: mux21c port map ( 
        a=>input_tb(0),
        b=>input_tb(1),
        s=>input_tb(2),
        y=>y_tb
    );
    
    stimuli: process
      begin

        input_tb <= (others => '0');
        wait for 50 ns;
        loop
            input_tb <= input_tb+1;
            wait for 50 ns;
        end loop;

    end process;
    
end v1;