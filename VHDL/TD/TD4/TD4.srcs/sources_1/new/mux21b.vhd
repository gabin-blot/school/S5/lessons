library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity mux21b is
port(
    a: in std_logic;
    b: in std_logic;
    s: in std_logic;
    y: out std_logic
);
end mux21b;

architecture v1 of mux21b is
  begin
    p1 : process(a,b,s)
      begin
        if s = '0' then y <= a ;
        else y <= b ;
        end if ;
    end process ;
end v1;