library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity mux21_top is
    port(
        sw: in std_logic_vector(1 downto 0);
        btnC: in std_logic_vector(0 downto 0);
        led: out std_logic_vector(0 downto 0)
    );
end mux21_top;

architecture Behavioral of mux21_top is
    component mux21b
        port(
            a: in std_logic;
            b: in std_logic;
            s: in std_logic;
            y: out std_logic
        );
    end component ;
      begin
        c1 : mux21b port map( a=> sw(0), b => sw(1), s => btnC(0), y=> led(0)) ;
end Behavioral;