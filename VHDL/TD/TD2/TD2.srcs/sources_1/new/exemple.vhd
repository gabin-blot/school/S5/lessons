------------------------------------
--Premier exemple

LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY exemple IS
    PORT (
        a,b: in std_logic;
        s: out std_logic
    );
END exemple;

ARCHITECTURE v1 OF exemple IS
  BEGIN
    s <= a and b;
END v1;