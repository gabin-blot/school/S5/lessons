------------------------------------
--Component 1

library ieee;
use ieee.std_logic_1164.all;

entity C2 is
    port (
        a,b,rin: in std_logic; 
        s, rout: out std_logic
    );
end C2;

architecture v1 of c2 is

    component c1
        port (
            e1,e2: in std_logic; 
            s1,s2: out std_logic
        );
    end component;
    
    signal n1, n2, n3: std_logic;
      begin
        inst1: c1 port map( e1=>a,e2=>b,s1=>n1,s2=>n2);
        inst2: c1 port map (e1=>n1,e2=>rin,s1=>s,s2=>n3);
        rout <= n2 or n3 ;
end v1;