------------------------------------
--Testbench of the component 3 that is a 4 bit adder
library ieee;
use ieee.std_logic_1164.all;

entity Q1_add4_Testbench is
end Q1_add4_Testbench;

architecture v1 of Q1_add4_Testbench is
    
    signal rin: std_logic;
    signal s_a, s_b: std_logic_vector ( 3 downto 0 );
    signal s_s: std_logic_vector ( 4 downto 0 );
    
    component add4
    port(
        r0:in std_logic;
        a,b: in std_logic_vector ( 3 downto 0);
        s : out std_logic_vector(4 downto 0)
    );
    end component;
    
  begin
  
    uut: add4 port map ( 
        a=>s_a,
        b=>s_b,
        r0=>'0',
        s=>s_s
    );
    
    stimuli: process
      begin
        s_a<="0001"; s_b <="0001"; wait for 30 ns;
        s_a<="0010"; s_b <="0010"; wait for 30 ns;
        s_a<="0100"; s_b <="0010"; wait for 30 ns;
        s_a<="1000"; s_b <="1100"; wait for 30 ns;
        s_a<="0000"; s_b <="0000"; wait for 30 ms;

    end process;
end v1;