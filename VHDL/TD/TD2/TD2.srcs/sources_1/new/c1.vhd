------------------------------------
--Component 1

LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY C1 IS
    PORT (
        e1,e2: in std_logic;
        s1, s2: out std_logic
    );
END C1;

ARCHITECTURE v1 OF C1 IS
  BEGIN
    s1 <= e1 xor e2;
    s2 <= e1 and e2;
    
END v1;