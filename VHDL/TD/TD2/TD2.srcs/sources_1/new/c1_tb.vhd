------------------------------------
--Testbench of the component
LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY C1_Testbench IS
END C1_Testbench;

ARCHITECTURE v1 OF C1_Testbench IS

    SIGNAL entree1, entree2, sortie1, sortie2: std_logic;
    
    COMPONENT C1
        PORT (
        e1,e2: in std_logic;
        s1,s2: out std_logic);
    END COMPONENT;
    
  BEGIN
  
    -- Instantiate the Unit Under Test (UUT)
    uut: C1 port map (e1 => entree1, e2 => entree2, s1 => sortie1, s2 => sortie2);
    stimuli:process
    
    BEGIN
        entree1<='0';
        entree2<='0';
          wait for 30 ns;

        entree2<='1';
          wait for 30 ns;
        
        entree1<='1';
        entree2<='0';
          wait for 30 ns;

        entree2<='1';
          wait for 30 ns;
    END process;
END v1;