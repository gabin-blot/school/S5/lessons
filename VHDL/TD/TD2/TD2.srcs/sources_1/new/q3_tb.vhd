------------------------------------
--Testbench of the component 3 that is a 4 bit adder
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity Q3_adder_Testbench is
  generic(M: integer := 4);
end Q3_adder_Testbench;

architecture v1 of Q3_adder_Testbench is


    signal tb_input: std_logic_vector (2*M-1 downto 0);
    signal tb_output: std_logic_vector (M-1 downto 0);
    signal tb_cf, tb_ovf: std_logic;

    component adder
        port(
            a,b: in std_logic_vector ( M-1 downto 0 );
            s: out std_logic_vector ( M-1 downto 0 );
            cf, ovf: out std_logic
        );
    end component;

  begin

    uut: adder port map (
        a=> tb_input(2*M-1 downto M),
        b=> tb_input(M -1 downto 0),
        s=> tb_output,
        cf=>tb_cf,
        ovf=>tb_ovf
    );

    stimuli: process
      begin

        tb_input <= (others => '0');
        wait for 50 ns;
        loop
            tb_input <= tb_input+1;
            wait for 50 ns;
        end loop;



    end process;
end v1;
