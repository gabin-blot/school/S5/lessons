-----------------------------------
--Component for question 2

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity addN is
    generic(N: integer := 4);
    port ( 
        a,b: in std_logic_vector ( N-1 downto 0 );
        s: out std_logic_vector ( N-1 downto 0 )
        );
end addN;

architecture v1 of addN is
 begin
    process(a,b)
        begin
            s <= a + b ;
        end process ;
end v1;
-----------------------------------