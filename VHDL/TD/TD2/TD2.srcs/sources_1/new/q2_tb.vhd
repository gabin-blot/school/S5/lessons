------------------------------------
--Testbench of the component 3 that is a 4 bit adder
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity Q2_addN_Testbench is
    generic(M: integer := 4);
end Q2_addN_Testbench;

architecture v1 of Q2_addN_Testbench is
     
    
    signal entree: std_logic_vector (2*M-1 downto 0);
    signal sortie: std_logic_vector (M-1 downto 0);

     
    component addN
    port(
        a,b: in std_logic_vector ( 3 downto 0);
        s: out std_logic_vector ( 3 downto 0 )
    );
    end component;
    
  begin
  
    uut: addN port map ( 
        a=> entree(2*M-1 downto M),
        b=> entree(M -1 downto 0),
        s=>sortie
    );
    
    stimuli: process
      begin
      
        entree <= (others => '0');
        wait for 50 ns;
        loop
            entree <= entree+1;
            wait for 50 ns;
        end loop;

    end process;
end v1;