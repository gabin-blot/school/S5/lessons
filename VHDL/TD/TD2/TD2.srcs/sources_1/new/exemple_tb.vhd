------------------------------------
--Testbench
LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY exemple_Testbench IS
END exemple_Testbench;

ARCHITECTURE v1 OF exemple_Testbench IS

    SIGNAL entree1, entree2, sortie: std_logic;
    
    COMPONENT exemple
        PORT (
        a,b: in std_logic;
        s: out std_logic);
    END COMPONENT;
    
  BEGIN
    
    uut: exemple port map (a => entree1, b=> entree2, s => sortie);
    stimuli:process
    
    BEGIN
        entree1<='0';
        entree2<='0';
          wait for 30 ns;

        entree2<='1';
          wait for 30 ns;
        
        entree1<='1';
        entree2<='0';
          wait for 30 ns;

        entree2<='1';
          wait for 30 ns;
    END process;
END v1;