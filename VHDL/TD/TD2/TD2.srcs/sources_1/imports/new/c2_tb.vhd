------------------------------------
--Testbench of the component 2
library ieee;
use ieee.std_logic_1164.all;

entity C2_Testbench is
end C2_Testbench;

architecture v1 of C2_Testbench is
    
    signal bit1, bit2, ret_e, ret_s, som: std_logic;
    
    component c2
     port(a,b,rin: in std_logic; s, rout: out std_logic);
    end component;
    
  begin
  
    uut: C2 port map ( 
        a=>bit1,
        b=>bit2,
        rin=>ret_e,
        s=>som,
        rout=>ret_s
    );
    
    stimuli: process
      begin
        ret_e<='0'; bit1<='0'; bit2 <='0';
        wait for 30 ns;
        bit1<='1'; wait for 30 ns;
        bit2<='1';
        wait for 30 ns;
        ret_e<='1';
        wait for 30 ns;
    end process;
end v1;