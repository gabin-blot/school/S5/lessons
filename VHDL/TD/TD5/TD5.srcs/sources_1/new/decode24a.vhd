-- Decodeur 2 vers 4 A ------------------------------

library ieee ;
use ieee.std_logic_1164.all;

entity decode2to4a is
    port( 
        a: in std_logic_vector(1 downto 0);
        y: out std_logic_vector(3 downto 0)
    );
end decode2to4a;

-----------------------------------------------------

architecture v1 of decode2to4a is
  begin
    y(0)<= not a(1) and not a(0);
    y(1)<= not a(1) and a(0);
    y(2)<= a(1) and not a(0);
    y(3)<= a(1) and a(0);
end v1;