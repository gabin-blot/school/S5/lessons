-- Decodeur 3 vers 8 --------------------------------

library ieee ;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity decode3to8 is
    port( 
        a: in std_logic_vector(2 downto 0);
        y: out std_logic_vector(7 downto 0)
    );
end decode3to8;

-----------------------------------------------------

architecture v1 of decode3to8 is
  begin
      p1: process(a)
          begin
            for i in 0 to 7 loop
                if (i = conv_integer(a)) then
                    y(i)<='1';
                else
                    y(i)<='0';
                end if;
            end loop;
    end process p1;
end v1;