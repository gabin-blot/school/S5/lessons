-- Comparateur

library ieee;
use ieee.std_logic_1164.all;

entity compN is
generic(N: integer := 4);
port(
    a: in std_logic_vector(N-1 downto 0);
    b: in std_logic_vector(N-1 downto 0);
    sup: out std_logic;
    eq: out std_logic;
    inf: out std_logic
);
end compN;

architecture v1 of compN is
    begin
    p1 : process(a,b)
      begin
        if a > b then
            sup <= '1';
            inf <= '0'; 
            eq <= '0'; 
        elsif a < b then 
            sup <= '0'; 
            inf <= '1' ;
            eq <= '0'; 
        elsif a = b then 
            sup <= '0'; 
            inf <= '0'; 
            eq <= '1' ;
        else
            sup <= '0'; 
            inf <= '0'; 
            eq <= '0'; 
        end if ;
    end process ;
    
end v1;