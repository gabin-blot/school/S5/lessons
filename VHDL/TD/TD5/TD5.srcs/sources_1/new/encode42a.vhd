---- Encodeur 4 vers 2 A -----------------------------

library ieee ;
use ieee.std_logic_1164.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

-----------------------------------------------------

entity encode4to2a is
    port( 
        x: in std_logic_vector(3 downto 0);
        y: out std_logic_vector(1 downto 0);
        valid: out std_logic
    );
end encode4to2a;
-----------------------------------------------------
architecture v1 of encode4to2a is
begin
    process(x)
        variable valid_var: std_logic_vector(1 downto 0);
          begin
            valid<='0';
            valid_var := "00";
            y<="00";
            for i in 0 to 3 loop
                valid_var := valid_var + x(i);
                if (x(i)='1') then
                    y <= conv_std_logic_vector(i,2);
                end if;
            end loop;
            
            if (valid_var = "01") then
                valid <= '1';
            else        
                valid <= '0';
            end if;
             
    end process;
end v1;