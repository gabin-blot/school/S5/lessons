-- Top Decodeur 3 vers 8 -----------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity encode8to3_top is
    port(
        sw: in std_logic_vector( 7 downto 0 );
        
        led : out STD_LOGIC_VECTOR ( 15 downto 0);
        
        an: out std_logic_vector ( 3 downto 0);
        dp: out std_logic
    );
end encode8to3_top;

-----------------------------------------------------

architecture v1 of encode8to3_top is
    
    component encode8to3
        port( 
            a: in std_logic_vector(2 downto 0);
            y: out std_logic_vector(7 downto 0);
            valid: out std_logic
        );
    end component;

    begin
        uut: encode8to3 port map ( 
            a=>sw,
            y=>led,
            valid=>dp
        );
end v1;