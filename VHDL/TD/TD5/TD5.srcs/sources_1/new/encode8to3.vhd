---- Encodeur 8 vers 3 -----------------------------

library ieee ;
use ieee.std_logic_1164.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

-----------------------------------------------------

entity encode8to3 is
    port( 
        x: in std_logic_vector(7 downto 0);
        y: out std_logic_vector(2 downto 0);
        valid: out std_logic
    );
end encode8to3;
-----------------------------------------------------
architecture v1 of encode8to3 is
begin
    process(x)
        variable valid_var: std_logic_vector(2 downto 0);
          begin
            valid<='0';
            valid_var := (others => '0');
            y<= (others => '0');
            for i in 0 to 7 loop
                valid_var := valid_var + x(i);
                if (x(i)='1') then
                    y <= conv_std_logic_vector(i,3);
                end if;
            end loop;
            
            if (valid_var = "01") then
                valid <= '1';
            else        
                valid <= '0';
            end if;
             
    end process;
end v1;