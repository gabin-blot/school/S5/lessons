-- Decodeur 2 vers 4 B ------------------------------

library ieee ;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity decode2to4b is
    port( 
        a: in std_logic_vector(1 downto 0);
        y: out std_logic_vector(3 downto 0)
    );
end decode2to4b;

-----------------------------------------------------

architecture v1 of decode2to4b is
  begin
      p1: process
          begin
            for i in 0 to 3 loop
                if (i = conv_integer(a)) then
                    y(i)<='1';
                else
                    y(i)<='0';
                end if;
            end loop;
    end process p1;
    
end v1;