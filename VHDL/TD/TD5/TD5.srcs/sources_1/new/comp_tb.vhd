-- Testbench comparateur

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity compN_Testbench is
    generic(N: integer := 4);
end compN_Testbench;

architecture v1 of compN_Testbench is

    signal sup_tb, inf_tb, eq_tb: std_logic;
    signal input_tb: std_logic_vector((2*N)-1 downto 0);
    
    component compN
        generic(N: integer := N);
        port(
            a: in std_logic_vector(N-1 downto 0);
            b: in std_logic_vector(N-1 downto 0);
            sup: out std_logic;
            eq: out std_logic;
            inf: out std_logic
        );
    end component;

    begin
        uut: compN port map ( 
            a=>input_tb((2*N)-1 downto N),
            b=>input_tb(N-1 downto 0),
            sup=>sup_tb,
            eq=>eq_tb,
            inf=>inf_tb
        );
        
        stimuli: process
          begin
    
            input_tb <= (others => '0');
            wait for 50 ns;
            loop
                input_tb <= input_tb+1;
                wait for 50 ns;
            end loop;

    end process;
end v1;