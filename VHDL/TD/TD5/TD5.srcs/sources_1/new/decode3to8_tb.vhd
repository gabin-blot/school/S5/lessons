-- Testbench Decodeur 3 vers 8 -----------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity decode3to8_Testbench is
end decode3to8_Testbench;

-----------------------------------------------------

architecture v1 of decode3to8_Testbench is

    signal a_tb: std_logic_vector(2 downto 0);
    signal y_tb: std_logic_vector(7 downto 0);
    
    component decode3to8
        port( 
            a: in std_logic_vector(2 downto 0);
            y: out std_logic_vector(7 downto 0)
        );
    end component;

    begin
        uut: decode3to8 port map ( 
            a=>a_tb,
            y=>y_tb
        );
        
        stimuli: process
          begin
    
            a_tb <= (others => '0');
            wait for 50 ns;
            loop
                a_tb <= a_tb+1;
                wait for 50 ns;
            end loop;

    end process;
end v1;