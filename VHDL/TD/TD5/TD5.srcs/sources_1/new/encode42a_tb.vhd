-- Testbench Encodeur 4 vers 2 A -----------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity encode4to2a_Testbench is
end encode4to2a_Testbench;

-----------------------------------------------------

architecture v1 of encode4to2a_Testbench is

    signal x_tb: std_logic_vector(3 downto 0);
    signal y_tb: std_logic_vector(1 downto 0);
    signal valid_tb: std_logic;
    
    component encode4to2a
        port( 
            x: in std_logic_vector(3 downto 0);
            y: out std_logic_vector(1 downto 0);
            valid: out std_logic
        );
    end component;

    begin
        uut: encode4to2a port map ( 
            x=>x_tb,
            y=>y_tb,
            valid=>valid_tb
        );
        
        stimuli: process
          begin
    
            x_tb <= (others => '0');
            wait for 50 ns;
            loop
                x_tb <= x_tb+1;
                wait for 50 ns;
            end loop;

    end process;
end v1;