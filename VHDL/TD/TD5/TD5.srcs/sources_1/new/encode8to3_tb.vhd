-- Testbench Encodeur 8 vers 3 -----------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity encode8to3_Testbench is
end encode8to3_Testbench;

-----------------------------------------------------

architecture v1 of encode8to3_Testbench is

    signal x_tb: std_logic_vector(7 downto 0);
    signal y_tb: std_logic_vector(2 downto 0);
    signal valid_tb: std_logic;
    
    component encode8to3
        port( 
            x: in std_logic_vector(7 downto 0);
            y: out std_logic_vector(2 downto 0);
            valid: out std_logic
        );
    end component;

    begin
        uut: encode8to3 port map ( 
            x=>x_tb,
            y=>y_tb,
            valid=>valid_tb
        );
        
        stimuli: process
          begin
    
            x_tb <= (others => '0');
            wait for 50 ns;
            loop
                x_tb <= x_tb+1;
                wait for 50 ns;
            end loop;

    end process;
end v1;