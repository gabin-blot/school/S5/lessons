
#include <stdio.h>
#include <string.h>
#include <time.h>



void main(void) {

  srand (time(0));

  int iMax = 1;
  int jMax = 1;

  do {
    iMax = (int) rand()%5;
  } while(iMax < 2);

  do {
    jMax = (int) rand()%5;
  } while(jMax < 2);

  int isMax, isMin;

  int A[iMax][jMax];
  int Max[iMax][jMax];
  int Min[iMax][jMax];
  int ptclos[iMax][jMax];

  int parser=0;

  for (int i = 0; i < iMax; i++) {
    for (int j = 0; j < jMax; j++) {
      A[i][j] = (int) rand()%9;
      parser++;
    }
  }

  printf("\nA :\n");
  for (int i = 0; i < iMax; i++) {
    for (int j = 0; j < jMax; j++) {
      printf("%d ", A[i][j]);
    }
    printf("\n");
  }


  for (int i = 0; i < iMax; i++) {
    isMax = 0;

    for (int j = 0; j < jMax; j++) {
      if(A[i][j] > isMax)
        isMax = A[i][j];
    }
    for (int j = 0; j < jMax; j++) {
      Max[i][j] = (A[i][j] == isMax);
    }
  }

  printf("\nMax :\n");
  for (int i = 0; i < iMax; i++) {
    for (int j = 0; j < jMax; j++) {
      printf("%d ", Max[i][j]);
    }
    printf("\n");
  }


  for (int j = 0; j < jMax; j++) {
    isMin = 10;
    for (int i = 0; i < iMax; i++) {
      if(A[i][j] < isMin)
        isMin = A[i][j];
      }
    for (int i = 0; i < iMax; i++) {
      Min[i][j] = (A[i][j] == isMin);
    }
  }


  printf("\nMin :\n");
  for (int i = 0; i < iMax; i++) {
    for (int j = 0; j < jMax; j++) {
      printf("%d ", Min[i][j]);
    }
    printf("\n");
  }


  for (int i = 0; i < iMax; i++) {
    for (int j = 0; j < jMax; j++) {
      if(Max[i][j] && Min[i][j])
        ptclos[i][j] = 1;
      else
        ptclos[i][j] = 0;
    }
  }

  printf("\nPoints-clos :\n");
  for (int i = 0; i < iMax; i++) {
    for (int j = 0; j < jMax; j++) {
      printf("%d ", ptclos[i][j]);
    }
    printf("\n");
  }

}
