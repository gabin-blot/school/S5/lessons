#include <stdio.h>



int main(void) {
  int annee;
  scanf("%d", &annee);
  if((annee % 400 == 0) || (annee % 4 == 0 && annee % 100 != 0))
    printf("Année bissexstile !");
  else
    printf("\nAnnée normale...\n");
  return 0;
}
