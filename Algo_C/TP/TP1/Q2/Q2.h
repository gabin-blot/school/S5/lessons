void getParams(void);
void displayTiles(void);
void getLoopType(void);
void forLoop(void);
void whileLoop(void);
void doWhileLoop(void);
