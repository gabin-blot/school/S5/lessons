#include <stdio.h>
#include "Q2.h"

int p, n;
char C1, C2, loopType;

int main(int argc, char const *argv[]) {
  do {
    getParams();
    getLoopType();
    displayTiles();
  } while(n != 0);
  return 0;
}

void getParams(void) {

  do {
    printf("Nombre de cases sur le côté : " );
    scanf("\n%d", &n);
  } while(n > 9 || n%2);

  if (!n) {
    return;
  }

  do {
    printf("Nombre de caractères par case : ");
    scanf("\n%d", &p);
  } while(p > 9);

  printf("Caractère pour les cases noires : ");
  scanf("\n%c", &C1);

  do {
    printf("Caractère pour les cases blanches : ");
    scanf("\n%c", &C2);
  } while(C1 == C2);

}

void getLoopType(void) {
  loopType = 'd';
  while(loopType == 'd' && n) {
    printf("Type d'affichage de damier : \n");
    printf("  a - en utilisant des for\n");
    printf("  b - en utilisant des while\n");
    printf("  c - en utilisant des do while\n");
    printf("  d - rentrer les paremètres\n");
    scanf("\n%c", &loopType);
    if(loopType == 'd')
      getParams();
    };
}

void displayTiles(void) {
  switch (loopType) {
    case 'a':
      forLoop();
      break;

    case 'b':
      whileLoop();
      break;

    case 'c':
      doWhileLoop();
      break;
  }
}

void forLoop(void)  {
  for (int i = 0; i < p*n; i++) {
    if((i%(2*p)) < p)
    {
      for (int j = 0; j < p*n; j++) {
        if ((j%(2*p)) < p)
          printf("%c",C1);
        else
        printf("%c",C2);
      }
    }
    else
    {
      for (int j = 0; j < p*n; j++) {
        if ((j%(2*p)) < p)
        printf("%c",C2);
        else
        printf("%c",C1);
      }
    }
    printf("\n");
  }
}

void whileLoop (void) {
  int i = 0;
  int j = 0;

  while (i < p*n) {
    if((i%(2*p)) < p)
    {
      j = 0;
      while (j < p*n) {
        if ((j%(2*p)) < p)
          printf("%c",C1);
        else
        printf("%c",C2);
        j++;
      }
    }
    else
    {
      j = 0;
      while (j < p*n) {
        if ((j%(2*p)) < p)
          printf("%c",C2);
        else
        printf("%c",C1);
        j++;
      }
    }
    printf("\n");
    i++;
  }
}

void doWhileLoop (void) {
  int i = 0;
  int j;
  do {
    if((i%(2*p)) < p)
    {
      j = 0;
      do {
        if ((j%(2*p)) < p)
          printf("%c",C1);
        else
        printf("%c",C2);
        j++;
      } while(j < p*n);
    }
    else
    {
      j = 0;
      do {
        if ((j%(2*p)) < p)
          printf("%c",C2);
        else
        printf("%c",C1);
        j++;
      } while(j < p*n);
    }
    printf("\n");
    i++;
  } while(i < p*n);
}
