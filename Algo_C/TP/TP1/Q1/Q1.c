#include <stdio.h>
#include "Q1.h"

int p, n;
char C1, C2;

int main(int argc, char const *argv[]) {
  do {
    getParams();
    displayTiles();
  } while(n != 0);
  return 0;
}

void getParams(void) {
  int error;

  error = 0;
  do {
    if(error)
      printf("/!\\ : Le chiffre doit être entre 2 et 8 ainsi que paire\n");
    printf("Nombre de cases sur le côté : " );
    n = getchar() -'0';
    while (getchar() != '\n') {}  // On vide le buffer de caractères
    error++;
  } while((n > 9 || n%2 || n < 2) && n != 0);

  if (!n) {
    return;
  }

  error = 0;
  do {
    if(error)
      printf("/!\\ : Le chiffre doit être compris entre 1 et 9 \n");
    printf("Nombre de caractères par case : ");
    p = getchar() -'0';
    while (getchar() != '\n') {}  // On vide le buffer de caractères
    error++;
  } while(p < 1 || p > 9);

  printf("Caractère pour les cases noires : ");
  C1 = getchar();
  while (getchar() != '\n') {}  // On vide le buffer de caractères

  error=0;
  do {
    if(error)
      printf("/!\\ : Le caractère doit être different de : %c \n", C1);
    printf("Caractère pour les cases blanches : ");
    C2 = getchar();
    while (getchar() != '\n') {}  // On vide le buffer de caractères
    error++;
  } while(C1 == C2);

}

void displayTiles(void) {
  for (int i = 0; i < p*n; i++) {
    if((i%(2*p)) < p)
    {
      for (int j = 0; j < p*n; j++) {
        if ((j%(2*p)) < p)
          printf("%c",C1);
        else
        printf("%c",C2);
      }
    }
    else
    {
      for (int j = 0; j < p*n; j++) {
        if ((j%(2*p)) < p)
        printf("%c",C2);
        else
        printf("%c",C1);
      }
    }
    printf("\n");
  }
}
