var indexSectionsWithContent =
{
  0: "acdefghiklmrsu",
  1: "e",
  2: "cdefhlm",
  3: "acdeghklmru",
  4: "eikms",
  5: "em",
  6: "defm"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Macros"
};

