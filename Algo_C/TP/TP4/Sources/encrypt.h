/**
 * @file encrypt.c
 * @author Gabin Blot
 * @brief File containing TP4's encryptions functions definitions
 * @date 18 Dec 2019
 *
 * This file contains multiple functions headers and definitions in order to handle TP'4s
 * encryption methods
 */


typedef struct encryption *encryption_t;
typedef void (*method_t)(encryption_t *toEncrypt);

struct encryption {
  method_t method;
  char key[1024];
  char inputString[1024];
  char outputString[1024];
  int isAPath;
  int isDecryption;
};


void encrypt(encryption_t *toEncrypt);
void caesar(encryption_t *toEncrypt);
void rotation(encryption_t *toEncrypt);
void lut(encryption_t *toEncrypt);

void EncryptFile(encryption_t *toEncrypt);
void DecryptFile(encryption_t *toEncrypt);

char askDecrytion(encryption_t *toEncrypt);
char askType(encryption_t *toEncrypt);
char askMethod(encryption_t *toEncrypt);
char askKey(encryption_t *toEncrypt);
char askSpec(encryption_t *toEncrypt);
char askString(encryption_t *toEncrypt);
char askFilePath(encryption_t *toEncrypt);
char askEncryptedPath(encryption_t *toEncrypt);
char askoutputFileSuffix(encryption_t *toEncrypt);

char askDispFile(void);
char askDispFile(void);
