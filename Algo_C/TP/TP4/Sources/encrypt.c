/**
 * @file encrypt.c
 * @author Gabin Blot
 * @brief File containing TP4's encryptions functions definitions
 * @date 18 Dec 2019
 *
 * This file contains multiple functions definitions in order to handle TP'4s
 * encryption methods
 */
 #include <stdio.h>
 #include <string.h>
 #include <stdlib.h>

 #include "defines.h"
 #include "caesar.h"


#include "encrypt.h"
#include "caseChanger.h"

#include "caesar.h"
#include "lut.h"


void encrypt(encryption_t *toEncrypt) {
  encryption_t temp_toEncrypt = *toEncrypt;


  method_t method = temp_toEncrypt -> method;

  //method = rotation;

  if (debug == enabled) { printf("method:%p, rotation:%p, lut:%p, caesar:%p\n", method, rotation, lut , caesar); }


  if(temp_toEncrypt -> isAPath == false) {
    method(&temp_toEncrypt);
  }
  else if (temp_toEncrypt -> isAPath == true) {


    FILE *inputFile, *outputFile;
    inputFile=fopen(temp_toEncrypt -> inputString, "r+");
    outputFile = fopen("./out.txt", "w");

    if(inputFile == NULL || outputFile == NULL){
      printf("INPUT / OUTPUT Error while opening file\n");
      return;
    }



    encryption_t fileLine_toEncrypt = temp_toEncrypt;

    fileLine_toEncrypt -> method = method;

    char fChar = 0;
    int i = 0;

    fChar = fgetc(inputFile);

    strcpy(temp_toEncrypt -> outputString, "./out.txt");
    while(!feof(inputFile)) {

      if(fChar == '\n') {
        fileLine_toEncrypt -> inputString[i] = '\0';
        strcpy(fileLine_toEncrypt -> outputString, fileLine_toEncrypt -> inputString);
        method(&fileLine_toEncrypt);

        strcat(fileLine_toEncrypt -> outputString, "\n");

        fputs(fileLine_toEncrypt -> outputString, outputFile);

        i = 0;
      }
      else {
        fileLine_toEncrypt -> inputString[i] = fChar;
        i++;
      }
      fChar = fgetc(inputFile);

    }

    fclose(inputFile);
    fclose(outputFile);
  }

  *toEncrypt = temp_toEncrypt;
}


void caesar(encryption_t *toEncrypt) {
  encryption_t temp_toEncrypt = *toEncrypt;

  strcpy(temp_toEncrypt -> outputString, temp_toEncrypt -> inputString);

  if(temp_toEncrypt -> isDecryption == true)
    decryptLine(temp_toEncrypt -> outputString);
  else
    encryptLine(temp_toEncrypt -> outputString);

  *toEncrypt = temp_toEncrypt;
}

void rotation(encryption_t *toEncrypt) {
  encryption_t temp_toEncrypt = *toEncrypt;

  strcpy(temp_toEncrypt -> outputString, temp_toEncrypt -> inputString);

  if(temp_toEncrypt -> isDecryption == true)
    decryptLineSpec(temp_toEncrypt -> outputString, temp_toEncrypt -> key[0]);
  else
    encryptLineSpec(temp_toEncrypt -> outputString, temp_toEncrypt -> key[0]);

  *toEncrypt = temp_toEncrypt;
}

void lut(encryption_t *toEncrypt) {
  encryption_t temp_toEncrypt = *toEncrypt;

    if(checkKeySize(temp_toEncrypt -> key))
      if(checkKeyHasUniqueCaracters(temp_toEncrypt -> key)) {

        strcpy(temp_toEncrypt -> outputString, UPPER(temp_toEncrypt -> inputString));

        int parser = 0;
        char encryptedChar;
        while (temp_toEncrypt -> outputString[parser] != '\0') {

          char** LUT = genLUT(temp_toEncrypt -> key);

          encryptedChar = temp_toEncrypt -> outputString[parser];
          if( encryptedChar >= 'A' && encryptedChar <= 'Z' ) {
            if(temp_toEncrypt -> isDecryption == true) {
              int lutParser = 0;
              while ( (encryptedChar != LUT[1][lutParser]) && lutParser < 26) {
                  lutParser++;
                }

              encryptedChar = LUT[0][lutParser];
            }
            else
              encryptedChar = LUT[1][encryptedChar - 'A'];
          }

          temp_toEncrypt -> outputString[parser] = encryptedChar;
          parser++;
        }

      }


  *toEncrypt = temp_toEncrypt;

}
