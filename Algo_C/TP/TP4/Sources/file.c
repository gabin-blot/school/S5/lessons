/**
 * @file input.c
 * @author Gabin Blot
 * @brief File containing encryption/decryption of a string in ceasar rotation.
 * @date 14 Dec 2019
 *
 * This header file defines functions in order to encrypt and decrypt a sting
 * with the caesar rotation algorithm
 */

#include <stdio.h>
//#include "file.h"




char ** readFile (char* filePath) {
  char result[1024][1024];

  FILE *fp, *outputFile;
  fp=fopen(filePath, "r+");
  outputFile = fopen("./out", "r+");

  char fChar = 0;
  int line = 0;
  int i = 0;

  do {
    fChar = fgetc(fp);

    if(fChar == '\n') {
      result[line][i] = '\0';
      line++;
      i = 0;
    }
    else {
      result[line][i] = fChar;
      i++;
    }

  } while(!feof(fp));
  result[line][i++] = EOF;

  fclose(fp);
}
