#ifndef CLEAR_H_INCLUDED
#define CLEAR_H_INCLUDED

#include <stdlib.h>

void clear(){
    #if defined(__linux__) || defined(__unix__) || defined(__APPLE__)
        system("clear");
    #endif

    #if defined(_WIN32) || defined(_WIN64)
        system("cls");
    #endif
}

char getcharFlushed() {
  char result[1024];
  fgets(result, sizeof(result), stdin);
  return result[0];
}

#endif // CLEAR_H_INCLUDED
