/**
 * @file caesar.h
 * @author Gabin Blot
 * @brief File containing encryption/decryption of a string in ceasar rotation.
 * @date 24 Nov 2019
 *
 * This header file defines functions in order to encrypt and decrypt a string
 * with the caesar rotation algorithm
 */

 #ifndef _CAESAR_H
 #  define _CAESAR_H

 /**
  * @brief Encrypts the string @c line through Caesar's rotation algorithm with
  * an offset of 13
  * @param line Should be a @c char array adress wich is ended by @c \0
  * @return @c return is @c true if the algorithm was successful otherwise it's
  * @c false
  */
int encryptLine(char* line);


 /**
  * @brief Encrypts the string @c line through Caesar's rotation algorithm with
  * an offset of @c spec
  * @param line Should be a @c char array adress wich is ended by @c \0
  * @param spec Should be an integer and represent the offset used in the
  * rotation algorithm
  * @return @c return is @c true if the algorithm was successful otherwise it's
  * @c false
  */
int encryptLineSpec(char* line, int spec);


 /**
  * @brief Decrypts the string @c line wich was encrypted by Caesar's rotation
  * algorithm with an offset of @c 13
  * @param line Should be a @c char array address wich is ended by @c \0
  * rotation algorithm
  * @return @c return is @c true if the algorithm was successful otherwise it's
  * @c false
  */
int decryptLine(char* line);


 /**
  * @brief Decrypts the string @c line wich was encrypted by Caesar's rotation
  * algorithm with an offset of @c spec
  * @param line Should be a @c char array address wich is ended by @c \0
  * @param spec Should be an integer and represent the offset used in the
  * rotation algorithm
  * @return @c return is @c true if the algorithm was successful otherwise it's
  * @c false
  */
int decryptLineSpec(char* line, int spec);



#endif /* _CAESAR_H */
