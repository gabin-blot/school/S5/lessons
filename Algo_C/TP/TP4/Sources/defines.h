/**
 * @file defines.h
 * @author Gabin Blot
 * @brief File containing defines for the project
 * @date 11 Dec 2019
 *
 * This header file contains defines for the project
 */

   /*! \def debug
      \brief Enabling debug mode in the project
      This macro can be used to enable \c printf in order to debug the project
  */

#ifndef maxCharString
#define maxCharString 1024
#endif

#ifndef maxCharPath
#define maxCharPath maxCharString
#endif

   /*! \def true
      \brief \c true value definition
  */
  #ifndef true
  #define true 1
  #endif


  /*! \def false
     \brief \c false value definition
 */
  #ifndef false
  #define false 0
  #endif


  /*! \def enabled
     \brief \c enabled value definition
 */
  #ifndef enabled
  #define enabled true
  #endif

    /*! \def disabled
       \brief \c disabled value definition
   */
  #ifndef disabled
  #define disabled false
  #endif


/*! \def debug
 \brief Enabling debug mode in the project
 This macro can be used to enable \c printf in order to debug the project
*/
#ifndef debug
#define debug disabled
#endif

/*! \def successful
   \brief Define failed exit code
   This macro is used to define successful function exit code
*/
#ifndef successful
#define successful 0
#endif
