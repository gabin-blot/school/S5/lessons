 /**
  * @file caseChanger.h
  * @author Gabin Blot
  * @brief File containing string case changer headers.
  * @date 24 Nov 2019
  *
  * This header file defines functions in order to modify a string case
  */

char * UPPER(char * string);
