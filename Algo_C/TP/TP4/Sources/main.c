/**
 * @file main.c
 * @author Gabin Blot
 * @brief File containing main for the TP
 * @date 14 Dec 2019
 *
 * This file contains main function for TP4
 */

#include <stdio.h>
#include <stdlib.h>

#include "hmi.h"
#include "encrypt.h"
#include "lut.h"
#include "defines.h"

int main(int argc, char const *argv[]) {

  encryption_t toEncrypt;



  if(askDecrytion(&toEncrypt) == successful) {
    if(askType(&toEncrypt) == successful) {
      if(askMethod(&toEncrypt) == successful) {

        if(     toEncrypt -> method == lut
            ||  toEncrypt -> method == rotation
          ) {
            askKey(&toEncrypt);
          }
        if(  toEncrypt -> isAPath == true )
          askFilePath(&toEncrypt);
        else {
          askString(&toEncrypt);
        }

        encrypt(&toEncrypt);

      }
    }
  }

  printf("\n\n");

  if(toEncrypt -> method == rotation)
  printf("Spec : <%d>\n", toEncrypt->key[0]);

  if(toEncrypt -> method == lut)
  printf("Key : <%s>\n", toEncrypt->key);

  if(toEncrypt -> isAPath == true)
  {
    printf("Output path : ./out.txt\n");
  }
  else {
    printf("Input string : %s\n", toEncrypt->inputString);
    printf("Output string : %s\n", toEncrypt->outputString);
  }

  system("sleep 1");

  return 0;
}
