/**
* @file main.h
* @author Gabin Blot
* @brief File containing TP4's HMI headers and definitions
* @date 18 Dec 2019
*
* This file contains multiple functions definitions in order to handle TP'4s
* HMI and program's ending functions
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include "defines.h"
#include "encrypt.h"
#include "clear.h"

#include "caesar.h"


int hmi(void) {

  return 0;
}


char askDecrytion(encryption_t *toEncrypt) { char localdebug = disabled;
  char exitCode, answer;
  encryption_t temp_toEncrypt = *toEncrypt;

  clear();

  do {
    printf("\n\n");
    printf(" -----------------------------------------------------\n");
    printf(" | 1) Encryption                                     |\n");
    printf(" | 2) Decryption                                     |\n");
    printf(" |   x) To exit                                      |\n");
    printf(" -----------------------------------------------------\n");
    printf("\n\n Your choice ? ");

    answer = getcharFlushed();

    clear();

    if(localdebug && debug) { printf("answer = %c ", answer);  }

  } while((answer < '0' || answer > '2')
          && answer != 'x'
          && answer != 'X');

  switch (answer) {
    case '1':
      temp_toEncrypt -> isDecryption = false;
      exitCode = 0;
      break;

    case '2':
      temp_toEncrypt -> isDecryption = true;
      exitCode = 0;
      break;

    case 'x':
      exitCode = 1;
      break;

    case 'X':
      exitCode = 1;
      break;

    default:
      temp_toEncrypt -> isDecryption = false;
      exitCode = 1;
      break;
  }

  *toEncrypt = temp_toEncrypt;

  if(localdebug && debug) { printf("toEncrypt -> isDecryption = %d | --- Press enter to continue ", temp_toEncrypt -> isDecryption); getcharFlushed(); }


  return successful;
}

char askType(encryption_t *toEncrypt) { char localdebug = disabled;
  char exitCode, answer;
  encryption_t temp_toEncrypt = *toEncrypt;

  clear();

  char * prefix;
  if(temp_toEncrypt -> isDecryption == true)
    strcpy(prefix, "De");
  else
    strcpy(prefix, "En");

  do {
    printf("\n\n");
    printf(" -----------------------------------------------------\n");
    printf(" | 1) %scrypt a string                               |\n", prefix);
    printf(" | 2) %scrypt a file                                 |\n", prefix);
    printf(" |   x) To exit                                      |\n");
    printf(" -----------------------------------------------------\n");
    printf("\n\n Your choice ? ");

    answer = getcharFlushed();

    clear();

    if(localdebug && debug) { printf("answer = %c ", answer);  }

  } while((answer < '0' || answer > '2')
          && answer != 'x'
          && answer != 'X');

  switch (answer) {
    case '1':
      temp_toEncrypt -> isAPath = false;
      exitCode = 0;
      break;

    case '2':
      temp_toEncrypt -> isAPath = true;
      exitCode = 0;
      break;

    case 'x':
      exitCode = 1;
      break;

    case 'X':
      exitCode = 1;
      break;

    default:
      temp_toEncrypt -> isAPath = false;
      exitCode = 1;
      break;
  }

  *toEncrypt = temp_toEncrypt;

  if(localdebug && debug) { printf("toEncrypt -> isAPath = %d | --- Press enter to continue ", temp_toEncrypt -> isAPath); getcharFlushed(); }


  return successful;
}

char askMethod(encryption_t *toEncrypt) { char localdebug = enabled;
  char exitCode, answer;

  encryption_t temp_toEncrypt = *toEncrypt;

  clear();

  char * prefix;
  if(temp_toEncrypt -> isDecryption == true)
    strcpy(prefix, "De");
  else
    strcpy(prefix, "En");


  do {
    printf("\n\n");
    printf(" -----------------------------------------------------\n");
    printf(" | 1) %scrypt with caesar's rotation                 |\n", prefix);
    printf(" | 2) %scrypt with a specific rotation number        |\n", prefix);
    printf(" | 3) %scrypt with a key (Look-Up Table method)      |\n", prefix);
    printf(" |   x) To exit                                      |\n");
    printf(" -----------------------------------------------------\n");
    printf("\n\n Your choice ? ");

    answer = getcharFlushed();
    clear();

    if(localdebug && debug) { printf("answer = %c ", answer); }

  } while((answer < '0' || answer > '3')
          && answer != 'x'
          && answer != 'X');


  switch (answer) {
    case '1':
      temp_toEncrypt -> method = caesar;
      exitCode = successful;
      break;

    case '2':
      temp_toEncrypt -> method = rotation;
      exitCode = successful;
      break;

    case '3':
      temp_toEncrypt -> method = lut;
      exitCode = successful;
      break;

    default:
      exitCode = 1;
      break;

  }


  *toEncrypt = temp_toEncrypt;

  return exitCode;
}

char askString(encryption_t *toEncrypt) { char localdebug = disabled;
  clear();
  char temp[1024];
  encryption_t temp_toEncrypt = *toEncrypt;


  if(temp_toEncrypt -> isDecryption == true)
    printf("\n Type the string you want to decrypt then press Enter : ");
  else
    printf("\n Type the string you want to encrypt then press Enter : ");

  scanf("%[^\n]s", temp);   getchar();

  printf("%s\n", temp);


  strcpy(temp_toEncrypt -> inputString, temp);

  *toEncrypt = temp_toEncrypt;

  if(localdebug && debug) { printf("string = %s --- Press enter to continue ", temp_toEncrypt -> inputString); getcharFlushed(); }
  return successful;
}

char askKey(encryption_t *toEncrypt) { char localdebug = disabled;
  clear();
  encryption_t temp_toEncrypt = *toEncrypt;


  if((temp_toEncrypt -> method) == lut) {
    printf("\n Type the key then press Enter (key Should only have unique characters) : ");
    scanf("%[^\n]s", temp_toEncrypt -> key);   getchar();
  }
  else if ((temp_toEncrypt -> method) == rotation) {
    char key[3];
    printf("Enter rotation parameter :  ");
    scanf("%[^\n]s", key);  getchar();
    temp_toEncrypt -> key[0] = atoi(key);
    printf("%d\n", temp_toEncrypt -> key[0]);
  }

  *toEncrypt = temp_toEncrypt;

  if(localdebug && debug) { printf("key = %s --- Press enter to continue ", temp_toEncrypt -> key); getcharFlushed(); }
  return successful;
}

char askFilePath(encryption_t *toEncrypt) { char localdebug = disabled;
  clear();
  char temp[1024];
  encryption_t temp_toEncrypt = *toEncrypt;

  printf("\n Type the path to the file then press Enter : ");
  scanf("%[^\n]s", temp);  getchar();

  strcpy(temp_toEncrypt -> inputString, temp);


  if(localdebug && debug) { printf("string = %s --- Press enter to continue ", temp_toEncrypt -> inputString); getcharFlushed(); }
  *toEncrypt = temp_toEncrypt;

  return successful;
}
