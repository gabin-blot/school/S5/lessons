/**
 * @file lut.c
 * @author Gabin Blot
 * @brief File containing Look Up Table and LUT management functions.
 * @date 24 Nov 2019
 *
 * This file is containing functions in order to generate and manage
 * the LUT
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "defines.h"
#include "lut.h"
#include "caseChanger.h"


char** genLUT(char* key) {  int localdebug = false;
  static char* result[2];
  static char index[26];
  static char pass[26];

  static char* uKey;
  uKey = UPPER(key);

  if(checkKeySize(uKey) && checkKeyHasUniqueCaracters(uKey)) {
    for (int i = 0; i < 26; i++)
      index[i] = (char) (i + 'A');
    genPass(uKey, pass);
  }

  result[0] = index;
  result[1] = pass;

  return result;
}

char* genPass(char *key, char *pass) {  int localdebug = false;

  if(debug && localdebug)
    printf("{============= genPass =============\n\n");

  char isLetterUsed[26];
  for (int i = 0; i < 26; i++) {
    pass[i] = ' ';
    isLetterUsed[i] = 0;
  }

  int kSize = keySize(key);


  int parser = 0;
  int letterIndex = 0;
  do {
    if(parser < kSize) {
      pass[parser] = key[parser];
      isLetterUsed[key[parser]-'A'] = true;
      parser++;
    }
    else if(!isLetterUsed[letterIndex]) {
      pass[parser] = (char) (letterIndex + 'A');
      isLetterUsed[letterIndex] = true;
      parser++;
      letterIndex++;
    }
    else
      letterIndex++;

  } while(parser < 26);

  for (int i = 0; i < 26; i++) {
    if(debug && localdebug)
      printf("%c", pass[i]);
  }
  if(debug && localdebug)
    printf("\n");

  for (int i = 0; i < 26; i++) {
    if(debug && localdebug)
      if(isLetterUsed[pass[i]-'A'])
      printf("^");
    else
      printf(" ");
  }
  if(debug && localdebug)
    printf("\n");

  if(debug && localdebug)
    printf("\n}============= genPass ============= -> pass = %s\n\n", pass);

  return pass;
}

void dispLUT(char** lut) { int localdebug = false;
  for (int i = 0; i < 26; i++) {
    printf("%c", lut[0][i]);
  }
  printf("\n");
  for (int i = 0; i < 26; i++) {
    printf("%c", lut[1][i]);
  }
  printf("\n");
}

int keySize (char* key) { int localdebug = false;

  if(debug && localdebug)
    printf("{============= keySize =============\n\n");

  int size = 0;
  while (key[size] != '\0' /*&& size < 0xFFFF*/) {
    size++;
  }

  if(debug && localdebug)
    printf("}============= keySize ============= -> size = %d\n\n", size);

  return size;
}

int checkKeySize (char* key) { int localdebug = false;

  if(debug && localdebug)
    printf("{=========== checkKeySize ==========\n\n");

  int isKeyCorrect = false;

  int size = keySize(key);

  if (size > 0 && size < 27)
    isKeyCorrect = true;
  else
    isKeyCorrect = false;

  if(debug && localdebug)
    printf("}========== checkKeySize =========== -> size = %d, isKeyCorrect = %d\n\n", size, isKeyCorrect);

  return isKeyCorrect;
}

int checkKeyHasUniqueCaracters (char* key){ int localdebug = false;

  if(debug && localdebug)
    printf("{=== checkKeyHasUniqueCaracters ====\n\n");

  int isKeyCorrect = true;
  int isCharSame = false;

  int size = keySize(key);
  int parser = 0;

  while (key[parser] != '\0' && !isCharSame) {
    for (int i = parser+1; i < size; i++) {
      isCharSame |= (key[parser] == key[i]);
      if(debug && localdebug)
        printf("%c : %c = %d\n", key[parser], key[i], isCharSame);
    }
    parser++;
  }

  if (isCharSame == false)
    isKeyCorrect = true;
  else
    isKeyCorrect = false;

  if(debug && localdebug)
    printf("\n}=== checkKeyHasUniqueCaracters ==== -> isKeyCorrect = %d\n\n", isKeyCorrect);

  return isKeyCorrect;
}
