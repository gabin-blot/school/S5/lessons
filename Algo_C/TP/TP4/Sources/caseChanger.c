/**
 * @file caseChanger.c
 * @author Gabin Blot
 * @brief File containing string case changer functions.
 * @date 24 Nov 2019
 *
 * This file is containing functions in order to change a string case
 */

 #include "caseChanger.h"

#include <stdio.h>

char * UPPER(char * string){
  static char result[26];
  int parser = 0;
  while (string[parser] != '\0') {
    if(string[parser] >= 'a' && string[parser] <= 'z')
      result[parser] = (char) (string[parser] + ('A'-'a'));
    else
      result[parser] = (char) string[parser];
    parser++;
  }
  result[parser] = (char) string[parser];
  return result;
}
