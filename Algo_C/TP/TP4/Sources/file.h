/**
 * @file input.h
 * @author Gabin Blot
 * @brief File containing encryption/decryption of a string in ceasar rotation.
 * @date 14 Dec 2019
 *
 * This header file defines functions in order to encrypt and decrypt a sting
 * with the caesar rotation algorithm
 */



 char * readFile (char* filePath);
