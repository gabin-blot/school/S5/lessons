/**
 * @file caesar.c
 * @author Gabin Blot
 * @brief File containing encryption/decryption of a string in ceasar rotation.
 * @date 24 Nov 2019
 *
 * This file defines functions in order to encrypt and decrypt a string
 * with the caesar rotation algorithm
 */

#include <stdio.h>
#include "caesar.h"

int encryptLine(char *line) {
  int i = 0;
  while (line[i] != '\0' && i <= 1000) {
    if(line[i] >= 'a' && line[i] <= 'z')
      line[i] = (line[i]-'a'+13)%26 + 'a';
    if(line[i] >= 'A' && line[i] <= 'Z')
      line[i] = (line[i]-'A'+13)%26 + 'A';
    i++;
  }
  //i=1000;
  return line[i]=='\0';
}

int encryptLineSpec(char *line, int spec) {
  int i = 0;
  while (line[i] != '\0' && i <= 1000) {
    if(line[i] >= 'a' && line[i] <= 'z')
      line[i] = (line[i]-'a'+spec)%26 + 'a';
    if(line[i] >= 'A' && line[i] <= 'Z')
      line[i] = (line[i]-'A'+spec)%26 + 'A';
    i++;
  }
  //i=1000;
  return line[i]=='\0';
}


int decryptLine(char *line) {
  int i = 0;
  while (line[i] != '\0' && i <= 1000) {
    if(line[i] >= 'a' && line[i] <= 'z')
      if(line[i]-'a'-13 < 0)
        line[i] = (26 + (line[i]-'a'-13))%26 + 'a';
      else
        line[i] = (line[i]-'a'-13)%26 + 'a';
    if(line[i] >= 'A' && line[i] <= 'Z')
      if(line[i]-'A'-13 < 0)
        line[i] = (26 + (line[i]-'A'-13))%26 + 'A';
      else
        line[i] = (line[i]-'A'-13)%26 + 'A';
    i++;
  }
  return line[i]=='\0';
}

int decryptLineSpec(char *line, int spec) {
  int i = 0;
  while (line[i] != '\0' && i <= 1000) {
    if(line[i] >= 'a' && line[i] <= 'z')
      if(line[i]-'a'-spec < 0)
        line[i] = (26 + (line[i]-'a'-spec))%26 + 'a';
      else
        line[i] = (line[i]-'a'-spec)%26 + 'a';
    if(line[i] >= 'A' && line[i] <= 'Z')
      if(line[i]-'A'-spec < 0)
        line[i] = (26 + (line[i]-'A'-spec))%26 + 'A';
      else
        line[i] = (line[i]-'A'-spec)%26 + 'A';
    i++;
  }
  return line[i]=='\0';
}
