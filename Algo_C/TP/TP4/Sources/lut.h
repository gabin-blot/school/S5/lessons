/**
 * @file lut.h
 * @author Gabin Blot
 * @brief File containing Look-Up Table and LUT management headers.
 * @date 24 Nov 2019
 *
 * This header file defines functions in order to generate and manage
 * the LUT
 */

/**
  * @brief return a lookup table with the @c key
  * @param key Should be a @c char array address wich is ended by @c \0 and
  * doesn't have duplicate letters
  * @return @c return is a lookup table in order to encrypt a string through
  * caesar's rotation with a specific @c key
  */
char** genLUT(char* key);

/**
  * @brief diplay look-up table with the address @c lut
  * @param address Should be a @c pointer to a look-up table
  */
void dispLUT(char** lut);

char* genPass(char *key, char *pass);
int checkKeySize (char* key);
int checkKeyHasUniqueCaracters (char* key);
int keySize (char* key);
