#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

#define gridX 200
#define gridY 50
#define dispX gridX+2
#define dispY gridY+2
#define bulletTime 0
#define showExpansionNumbers 0
#define debug 0
#define bMax 4000

#define KNRM  "\x1B[0m"
#define WNRM  "\x1b[30m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

#define generationSleep 0//10


#define expansionChar ' '
#define expansionColor KBLU
#define expansionSleep 0//10

#define traceBackChar '*'
#define traceBackColor KRED
#define traceBackSleep 0//10




void initLab(void);
void dispLab(void);
int expansionPhase(void);
void traceBackPhase(void);


typedef enum
{
    NORMAL,
    BLOCKS,
    EXPANSION,
    TRACEBACK,
    ENDS
} e_color;

typedef struct  {
  int x;
  int y;
  int value;
  int character;
  e_color color;
}t_case;


#endif // MAIN_H_INCLUDED
