#include "queue.h"

void init(t_queue *q) {
    q -> head = q -> tail = 0;
}

void enqueue(t_queue *q, t_case tile)
{
  q -> item[(q -> tail)++] = tile;
  q -> tail %= 128;
}

t_case dequeue(t_queue *q)
{
  q -> head %= 128;
  return q -> item[(q -> head)++];
}

int isEmpty(t_queue *q)
{
    return (q -> tail) == (q -> head);
}
