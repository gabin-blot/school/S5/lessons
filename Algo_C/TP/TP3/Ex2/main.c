#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "main.h"
#include "clear.h"
#include "msleep.h"
#include "queue.h"


t_case start, end, blocks[bMax], lab[gridX][gridY], disp[dispX][dispY];



int main(int argc, char const *argv[]) {
  clear();
  printf("Generating...\n\n");
  msleep(1000);
  initLab();
  //getchar();

  //printf("Let the Battle Begin!\n");
  //printf("Press Enter to Continue\n");

  if(expansionPhase())
    traceBackPhase();
  else
    printf("No path found !\n");
  dispLab();
  return 0;
}

void initLab(void) {
  int genError;

  // Labyrinth borders init
  for (int i = 0; i < dispY; i++) {
    disp[0][i].character = disp[dispX-1][i].character = '|';   // y axis borders
    disp[0][i].color = disp[dispX-1][i].color = NORMAL;   // y axis borders
  }


  for (int i = 0; i < dispX; i++) {
    disp[i][0].character = disp[i][dispY-1].character = '-';   // x axis bordes
    disp[i][0].color = disp[i][dispY-1].color = NORMAL;   // x axis bordes
  }

  // Generating blank cases
  for (int i = 0; i < gridX; i++) {
    for (int j = 0; j < gridY; j++) {
      lab[i][j].character = ' ';
      lab[i][j].color = NORMAL;
      lab[i][j].value = 0;
      lab[i][j].x = i;
      lab[i][j].y = j;
    }
  }

  // Generating block

  srand (time(0));
  int xRand, yRand;

  for (int i = 0; i < bMax; i++) {
    xRand = rand()%(gridX);
    yRand = rand()%(gridY);
    blocks[i].x = xRand;
    blocks[i].y = yRand;
    lab[xRand][yRand].character = '#';
    lab[xRand][yRand].color = BLOCKS;
    lab[xRand][yRand].value = -1;
    if(!(i%100)) {
      msleep(generationSleep);
      //dispLab();
    }
  }

  do {
    genError = 0;

    xRand = rand()%(gridX-1)+1;
    yRand = rand()%(gridY-1)+1;

    for (int i = 0; i < bMax; i++) {
      if(blocks[i].x == xRand && blocks[i].y == yRand) {
        genError++;
        break;
      }
    }
  } while(genError);
  start.x = xRand;
  start.y = yRand;
  lab[xRand][yRand].character = 'D';
  lab[xRand][yRand].value = 1;
  lab[xRand][yRand].color = ENDS;

  do {
    genError = 0;

    xRand = (rand()%(gridX-1))+1;
    yRand = (rand()%(gridY-1))+1;


    for (int i = 0; i < bMax; i++) {
      if(blocks[i].x == xRand && blocks[i].y == yRand) {
        genError++;
        break;
      }
    }
  } while(genError);
  end.x = xRand;
  end.y = yRand;
  lab[xRand][yRand].character = 'A';
  lab[xRand][yRand].value = -2;
  lab[xRand][yRand].color = ENDS;
}

void dispLab(void) {
  clear();
  msleep(1);
  for (int y = 0; y < dispY; y++) {
    for (int x = 0; x < dispX; x++) {

      if(x > 0 && x < gridX+1 && y > 0 && y < gridY+1) {
        disp[x][y].character = lab[x-1][y-1].character;
        disp[x][y].color = lab[x-1][y-1].color;
      }

      switch (disp[x][y].color) {
        case NORMAL:
          printf("%s%c\x1b[0m", KNRM, disp[x][y].character);
          break;

        case BLOCKS:
          printf("%s%c\x1b[0m", WNRM, disp[x][y].character);
          break;

        case EXPANSION:
          printf("%s%c\x1b[0m", KYEL, disp[x][y].character);
          break;

        case TRACEBACK:
          printf("%s%c\x1b[0m", KGRN, disp[x][y].character);
          break;

        case ENDS:
          printf("%s%c\x1b[0m", KRED, disp[x][y].character);
          break;

        default:
          printf("%s%c\x1b[0m", KMAG, disp[x][y].character);
          break;

      }
    }
    printf("\n");
  }
}

int expansionPhase(void) {
  int oldValue = 0;
  t_queue pile;
  t_case temp;
  init(&pile);
  enqueue(&pile, start);

  int found = 0;
  do {
    temp = dequeue(&pile);

    if( lab[temp.x-1][temp.y].value == -2 ||
        lab[temp.x+1][temp.y].value == -2 ||
        lab[temp.x][temp.y-1].value == -2 ||
        lab[temp.x][temp.y+1].value == -2)
        found++;

    if(!found) {
      if(bulletTime && temp.value >= 1)  //remove pvsly displayed character
        lab[temp.x][temp.y].character = ' ';


      //Check west tile
      if(lab[temp.x-1][temp.y].value == 0 && temp.x-1 >= 0) { //if tile is blank AND within the grid
        lab[temp.x-1][temp.y].value = temp.value+1; //add value to utilisera
        enqueue(&pile, lab[temp.x-1][temp.y]); // add new tile to the queue

        if(showExpansionNumbers)  //Show expansion character for debug
          lab[temp.x-1][temp.y].character = temp.value+1+'0';
        else { //Otherwise show the bullet
          lab[temp.x-1][temp.y].character = expansionChar;
          lab[temp.x-1][temp.y].color = EXPANSION;
        }
      }

      //Check east tile
      if(lab[temp.x+1][temp.y].value == 0 && temp.x+1 < gridX) { //if tile is blank AND within the grid
        lab[temp.x+1][temp.y].value = temp.value+1; //add value to utilisera
        enqueue(&pile, lab[temp.x+1][temp.y]); // add new tile to the queue

        if(showExpansionNumbers)  //Show expansion character for debug
          lab[temp.x+1][temp.y].character = temp.value+1+'0';
        else { //Otherwise show the bullet
          lab[temp.x+1][temp.y].character = expansionChar;
          lab[temp.x+1][temp.y].color = EXPANSION;
        }
      }

      //Check north tile
      if(lab[temp.x][temp.y-1].value == 0 && temp.y-1 >= 0) { //if tile is blank AND within the grid
        lab[temp.x][temp.y-1].value = temp.value+1; //add value to utilisera
        enqueue(&pile, lab[temp.x][temp.y-1]); // add new tile to the queue

        if(showExpansionNumbers)  //Show expansion character for debug
          lab[temp.x][temp.y-1].character = temp.value+1+'0';
        else { //Otherwise show the bullet
          lab[temp.x][temp.y-1].character = expansionChar;
          lab[temp.x][temp.y-1].color = EXPANSION;
        }
      }

      //Check south tile
      if(lab[temp.x][temp.y+1].value == 0 && temp.y+1 < gridY) { //if tile is blank AND within the grid
        lab[temp.x][temp.y+1].value = temp.value+1; //add value to utilisera
        enqueue(&pile, lab[temp.x][temp.y+1]); // add new tile to the queue

        if(showExpansionNumbers)  //Show expansion character for debug
          lab[temp.x][temp.y+1].character = temp.value+1+'0';
        else { //Otherwise show the bullet
          lab[temp.x][temp.y+1].character = expansionChar;
          lab[temp.x][temp.y+1].color = EXPANSION;
        }
      }
    }


    if(debug) {// && !(temp.value)){
      getchar();
      //dispLab();
    }
    else if(oldValue != temp.value) {
      oldValue = temp.value;
      msleep(expansionSleep);
      //dispLab();
    }

    end.value = temp.value+1;
  } while(!found && !isEmpty(&pile));
  //dispLab();

  if(isEmpty(&pile) && (end.value != -2))
    return 0;
  else
    return 1;
}

void traceBackPhase(void) {
  int parser = end.value;
  int x = end.x;
  int y = end.y;
  do {
    if(lab[x-1][y].value == parser) {
      lab[x-1][y].value = -2;
      lab[x-1][y].character = traceBackChar;
      lab[x-1][y].color = TRACEBACK;
      x--;
    }
    else if(lab[x+1][y].value == parser) {
      lab[x+1][y].value = -2;
      lab[x+1][y].character = traceBackChar;
      lab[x+1][y].color = TRACEBACK;
      x++;
    }
    else if(lab[x][y-1].value == parser) {
      lab[x][y-1].value = -2;
      lab[x][y-1].character = traceBackChar;
      lab[x][y-1].color = TRACEBACK;
      y--;
    }
    else if(lab[x][y+1].value == parser) {
      lab[x][y+1].value = -2;
      lab[x][y+1].character = traceBackChar;
      lab[x][y+1].color = TRACEBACK;
      y++;
    }
    parser--;
    if(debug)
      getchar();
    else
      msleep(traceBackSleep);

    //dispLab();

  } while(parser > start.value-1);
}
