#ifndef QUEUE_H_INCLUDED
#define QUEUE_H_INCLUDED

#include "main.h"

typedef struct  {
  t_case item[128];
  int head;
  int tail;
}t_queue;

void init(t_queue *q);
void enqueue(t_queue *q, t_case value);
t_case dequeue(t_queue *q);
int isEmpty(t_queue *q);
#endif // QUEUE_H_INCLUDED
