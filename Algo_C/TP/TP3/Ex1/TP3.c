#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


typedef struct  {
  int x;
  int y;
  int value;
  int character;
}t_case;

#include "TP3.h"

t_case start, end, blocks[bMax], lab[gridX][gridY], disp[dispX][dispY];



int main(int argc, char const *argv[]) {
  printf("Generating...\n\n");
  initLab();

  printf("Let the Battle Begin!\n");
  printf("Press Enter to Continue\n");
  //getchar();

  dispLab();
  expansionPhase();
  traceBackPhase();

  return 0;
}

void initLab(void) {
  int genError;

  // Labyrinth borders init
  for (int i = 0; i < dispY; i++) {
    disp[0][i].character = disp[dispX-1][i].character = '|';   // y axis borders
  }


  for (int i = 0; i < dispX; i++) {
    disp[i][0].character = disp[i][dispY-1].character = '-';   // x axis bordes
  }
  // Generating blank cases

  for (int i = 0; i < gridX; i++) {
    for (int j = 0; j < gridY; j++) {
      lab[i][j].character = ' ';
      lab[i][j].value = 0;
    }
  }

  // Generating block

  srand (time(0));
  int xRand, yRand;

  for (int i = 0; i < bMax; i++) {
    xRand = rand()%(gridX-1)+1;
    yRand = rand()%(gridY-1)+1;
    blocks[i].x = xRand;
    blocks[i].y = yRand;
    lab[xRand][yRand].character = '#';
    lab[xRand][yRand].value = -1;
  }

  do {
    genError = 0;

    xRand = rand()%(gridX-1)+1;
    yRand = rand()%(gridY-1)+1;

    for (int i = 0; i < bMax; i++) {
      if(blocks[i].x == start.x && blocks[i].y == start.y) {
        genError++;
        lab[xRand][yRand].character = ' ';
        lab[xRand][yRand].value = 0;
        break;
      }
    }

    start.x = xRand;
    start.y = yRand;
    lab[xRand][yRand].character = 'D';
    lab[xRand][yRand].value = 1;

  } while(genError);

  do {
    genError = 0;

    xRand = rand()%(gridX-1)+1;
    yRand = rand()%(gridY-1)+1;


    for (int i = 0; i < bMax; i++) {
      if(blocks[i].x == end.x && blocks[i].y == end.y) {
        genError++;
        lab[xRand][yRand].character = ' ';
        lab[xRand][yRand].value = 0;
        break;
      }
    }

    end.x = xRand;
    end.y = yRand;
    lab[xRand][yRand].character = 'A';
    lab[xRand][yRand].value = -2;

  } while(genError);

}


void dispLab(void) {
  clear();
  for (int y = 0; y < dispY; y++) {
    for (int x = 0; x < dispX; x++) {
      if(x > 0 && x < gridX+1 && y > 0 && y < gridY+1)
      disp[x][y].character = lab[x-1][y-1].character;

      printf("%c", disp[x][y].character);
    }
    printf("\n");
  }
}


void expansionPhase(void) {
  int found = 0;
  int parser = 1;
  do {
    dispLab();
    for (int x = 0 ; x< gridX; x++) {
      for (int y = 0 ; y < gridY; y++) {
        if(lab[x][y].value == parser) {

          if(bulletTime && parser > 1)
            lab[x][y].character = ' ';

          if(lab[x-1][y].value == 0 && x-1 >= 0)
          {
            lab[x-1][y].value = parser+1;
            if(showExpansionNumbers)
              lab[x-1][y].character = parser+1+'0';
            else
              lab[x-1][y].character = '.';
          }
          else if (lab[x-1][y].value == -2) {
            found++;
            break;
          }

          if(lab[x][y-1].value == 0 && y-1 >= 0)
          {
            lab[x][y-1].value = parser+1;
            if(showExpansionNumbers)
              lab[x][y-1].character = parser+1+'0';
            else
              lab[x][y-1].character = '.';
          }
          else if (lab[x][y-1].value == -2) {
            found++;
            break;
          }

          if(lab[x][y+1].value == 0 && y+1 < gridY)
          {
            lab[x][y+1].value = parser+1;
            if(showExpansionNumbers)
              lab[x][y+1].character = parser+1+'0';
            else
              lab[x][y+1].character = '.';
          }
          else if (lab[x][y+1].value == -2) {
            found++;
            break;
          }

          if(lab[x+1][y].value == 0 && x+1 < gridX)
          {
            lab[x+1][y].value = parser+1;
            if(showExpansionNumbers)
              lab[x+1][y].character = parser+1+'0';
            else
              lab[x+1][y].character = '.';
          }
          else if (lab[x+1][y].value == -2) {
            found++;
            break;
          }
        }
        if(found)
          break;
      }
    }
    end.value = parser;
    parser++;
    if(debug)
      getchar();
    else
      usleep(100000);

  } while(!found);
  printf("found : %d\n", parser);
}

void traceBackPhase(void) {
  int parser = end.value;
  int x = end.x;
  int y = end.y;
  do {
    dispLab();
    if(lab[x-1][y].value == parser) {
      lab[x-1][y].value = -2;
      lab[x-1][y].character = '*';
      x--;
    }
    else if(lab[x+1][y].value == parser) {
      lab[x+1][y].value = -2;
      lab[x+1][y].character = '*';
      x++;
    }
    else if(lab[x][y-1].value == parser) {
      lab[x][y-1].value = -2;
      lab[x][y-1].character = '*';
      y--;
    }
    else if(lab[x][y+1].value == parser) {
      lab[x][y+1].value = -2;
      lab[x][y+1].character = '*';
      y++;
    }
    parser--;

    if(debug)
    getchar();
    else
      usleep(100000);

  } while(parser > start.value);
}
