

#define gridX 40
#define gridY 20
#define dispX gridX+2
#define dispY gridY+2
#define bulletTime 1
#define showExpansionNumbers 0
#define debug 0
#define bMax 100


void initLab(void);
void dispLab(void);
void expansionPhase(void);
void traceBackPhase(void);

void clear(){
    #if defined(__linux__) || defined(__unix__) || defined(__APPLE__)
        system("clear");
    #endif

    #if defined(_WIN32) || defined(_WIN64)
        system("cls");
    #endif
}
