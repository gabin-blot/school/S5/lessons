#include <stdio.h>
#include "Ex2.h"

char buffer[64];

int main(int argc, char const *argv[]) {

  int size, notNumberErrorFlag, result, coef;

  do {
    notNumberErrorFlag = size = result = 0;
    coef = 1;

    printf("Entrer un nombre entier : ");
    scanf("%s", buffer);

    do {
      if (buffer[size] < '0' || buffer[size] > '9')
        notNumberErrorFlag = 1;
      size++;
    } while(buffer[size] != '\0');

    for (int i = size-1; i >= 0; i--) {
      result += (buffer[i]-'0')*coef;
      coef *= 10;
    }

    if (notNumberErrorFlag)
      printf("Erreur tout les caractères ne sont pas des chiffres\n");

  } while(notNumberErrorFlag);

  printf("Le nombre est %d\n", result);
  return 0;
}
