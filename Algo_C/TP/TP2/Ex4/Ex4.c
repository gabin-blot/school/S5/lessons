#include <stdio.h>
#include "Ex4.h"

char string1[64];
char string2[64];
char buffer[64];

int main(int argc, char const *argv[]) {
  getString();
  if(checkIfSame())
    printf("Les chaînes sont indentiques (%s : %s)\n",string1,string2);
  else
    getOrder();
  return 0;
}

void getString() {
  int flagStringError, parser;
  do {
    flagStringError = parser = 0;

    printf("Entrer le premier mot : ");
    scanf("%s", string1);

    while (string1[parser] != '\0') {
      if((string1[parser] >= 'A') && (string1[parser] <= 'Z'))
      {
        string1[parser] += ('a'-'A');
      }
      if((string1[parser] < 'a') || (string1[parser] > 'z'))
      {
        flagStringError++;
      }
      parser++;
    }

    if(flagStringError)
    printf("%d caractère(s) érroné(s) ! \n", flagStringError);

  } while(flagStringError);

  do {
    flagStringError = parser = 0;

    printf("Entrer le deuxième mot : ");
    scanf("%s", string2);

    while (string2[parser] != '\0') {
      if((string2[parser] >= 'A') && (string2[parser] <= 'Z'))
      {
        string2[parser] += ('a'-'A');
      }
      if((string2[parser] < 'a') || (string2[parser] > 'z'))
      {
        flagStringError++;
      }
      parser++;
    }

    if(flagStringError)
    printf("%d caractère(s) érroné(s) ! \n", flagStringError);

  } while(flagStringError);

}

int checkIfSame() {
  int parser = 0;
  int same = 1;
  do {
    same &= (string1[parser] == string2[parser]);
    parser++;
  } while(string1[parser] != '\0' && string2[parser] != '\0' && same);
  same &= (string1[parser] == string2[parser]);
  return same;
}

void getOrder() {
  int parser = 0;
  int orderDetermined = 0;
  do {
    if(string1[parser] < string2[parser])
      orderDetermined = 1;

    else if(string1[parser] > string2[parser])
      orderDetermined = 2;

    parser++;
  } while(!orderDetermined);

    if(orderDetermined == 1)
      printf("Ordre des chaînes : %s puis %s\n", string1, string2);
    if(orderDetermined == 2)
      printf("Ordre des chaînes : %s puis %s\n", string2, string1);

}
