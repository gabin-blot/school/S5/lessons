#include <stdio.h>
#include "Ex5.h"

char buffer[64];

int main(int argc, char const *argv[]) {
  getString();
  if(checkIfPalin())
  printf("La chaîne est un palindrome (%s)\n",buffer);
  else
  printf("La chaîne n'est pas un palindrome (%s)\n",buffer);

  return 0;
}

void getString() {
  int flagStringError, parser;

  do {
    printf("Entrer le premier mot : ");
    scanf("%[^\n]s", buffer);
    printf("Buffer : %s\n", buffer);
    flagStringError = parser = 0;

    while (buffer[parser] != '\0') {
      if((buffer[parser] >= 'A') && (buffer[parser] <= 'Z'))
      {
        buffer[parser] += ('a'-'A');
      }
      if((buffer[parser] < 'a' ||
        buffer[parser] > 'z') &&
        buffer[parser] != '.' &&
        buffer[parser] != ',' &&
        buffer[parser] != ';' &&
        buffer[parser] != ' ' &&
        buffer[parser] != ':' &&
        buffer[parser] != '!' &&
        buffer[parser] != '?'
      )
      {
        flagStringError++;
      }
    parser++;
  }

  if(flagStringError)
  printf("%d caractère(s) érroné(s) ! \n", flagStringError);

} while(flagStringError);

}

int checkIfPalin() {
  int size = 0;
  do {
    size++;
  } while(buffer[size] != '\0');
  printf("Size %d\n", size);

  int same = 1; // innocent until proven guilty
  for (int i = 0; i < (int) (size/2); i++) {
    same &= (buffer[i] == buffer[size-i-1]);
    printf("%d : %c - %c\n", i, buffer[i], buffer[size-i-1]);
  }

  return same;
}
