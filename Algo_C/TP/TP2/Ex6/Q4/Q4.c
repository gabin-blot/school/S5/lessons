#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Q4.h"

char string1[64];
char string2[64];
char buffer[64];

int main(int argc, char const *argv[]) {
  getString();
  int result = strcmp(string1, string2);
  if(!result)
    printf("Les chaînes sont indentiques (%s : %s)\n",string1,string2);
  else if(result < 0)
    printf("Les chaînes %s est avant %s\n",string1,string2);
  else
    printf("Les chaînes %s est avant %s\n",string2,string1);
  return 0;
}

void getString() {
  int flagStringError, parser;
  do {
    flagStringError = parser = 0;

    printf("Entrer le premier mot : ");
    scanf("%s", string1);

    while (string1[parser] != '\0') {
      if((string1[parser] >= 'A') && (string1[parser] <= 'Z'))
      {
        string1[parser] += ('a'-'A');
      }
      if((string1[parser] < 'a') || (string1[parser] > 'z'))
      {
        flagStringError++;
      }
      parser++;
    }

    if(flagStringError)
    printf("%d caractère(s) érroné(s) ! \n", flagStringError);

  } while(flagStringError);

  do {
    flagStringError = parser = 0;

    printf("Entrer le deuxième mot : ");
    scanf("%s", string2);

    while (string2[parser] != '\0') {
      if((string2[parser] >= 'A') && (string2[parser] <= 'Z'))
      {
        string2[parser] += ('a'-'A');
      }
      if((string2[parser] < 'a') || (string2[parser] > 'z'))
      {
        flagStringError++;
      }
      parser++;
    }

    if(flagStringError)
    printf("%d caractère(s) érroné(s) ! \n", flagStringError);

  } while(flagStringError);

}
