#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Q5.h"

char string[64];
char buffer[64];

int main(int argc, char const *argv[]) {
  getString();

  strcpy(buffer, string);
  strrev(buffer);

  if (!strcmp(string, buffer))
    printf("La chaîne est un palindrome (%s)\n",buffer);
  else
    printf("La chaîne n'est pas un palindrome (%s)\n",buffer);
  return 0;
}

void getString() {
  int flagStringError, parser;

  do {
    printf("Entrer le premier mot : ");
    scanf("%[^\n]s", string);
    flagStringError = parser = 0;

    while (string[parser] != '\0') {
      if((string[parser] >= 'A') && (string[parser] <= 'Z'))
      {
        string[parser] += ('a'-'A');
      }
      if((string[parser] < 'a' ||
      string[parser] > 'z') &&
      string[parser] != '.' &&
      string[parser] != ',' &&
      string[parser] != ';' &&
      string[parser] != ' ' &&
      string[parser] != ':' &&
      string[parser] != '!' &&
      string[parser] != '?'
    )
    {
      flagStringError++;
    }
    parser++;
  }

  if(flagStringError)
  printf("%d caractère(s) érroné(s) ! \n", flagStringError);

} while(flagStringError);

}
