#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Q1.h"

char buffer[64];

int main(int argc, char const *argv[]) {
  printf("Entrer une chaîne de caractères (max 64 caractères) : ");
  scanf("%s", buffer);
  printf("La chaîne fait %d caractères\n", (int) strlen(buffer));
  return 0;
}
