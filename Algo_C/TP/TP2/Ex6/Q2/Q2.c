#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Q2.h"

char buffer[64];

int main(int argc, char const *argv[]) {

  int size, notNumberErrorFlag;

  do {
    notNumberErrorFlag = 0;
    printf("Entrer un nombre entier : ");
    scanf("%s", buffer);

    do {
      if (buffer[size] < '0' || buffer[size] > '9') {
        notNumberErrorFlag++;
      }
      size++;
    } while(buffer[size] != '\0');

    if (notNumberErrorFlag)
    printf("Erreur tout les caractères ne sont pas que des chiffres\n");

  } while(notNumberErrorFlag);

  printf("Le nombre est %d\n", atoi(buffer));
  return 0;
}
