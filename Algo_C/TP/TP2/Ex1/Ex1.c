#include <stdio.h>
#include "Ex1.h"

char buffer[64];

int main(int argc, char const *argv[]) {
  printf("Entrer une chaîne de caractères (max 64 caractères) : ");
  scanf("%[^\n]s", buffer);
  int i;
  do {
    i++;
  } while(buffer[i] != '\0');
  printf("La chaîne fait %d caractères\n", i);
  return 0;
}
