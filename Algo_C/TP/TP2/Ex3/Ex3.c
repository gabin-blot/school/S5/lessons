#include <stdio.h>
#include "Ex3.h"

char buffer[64];

int main(int argc, char const *argv[]) {

  int number, coef;

  printf("Entrer un nombre entier : ");
  scanf("%d", &number);

  int temp = number;
  coef = 1;

  while(temp > 9) {
    coef *= 10;
    temp = (int) temp/10;
  };

  int j = 0;
  for (int i = coef; i > 0; i/=10) {
    buffer[j] = (int) (number/i) + '0';
    number = number % i;
    j++;
  }

  buffer[j] = '\0';

  printf("%s\n", buffer);
  return 0;
}
