/**
 * @file defines.h
 * @author Gabin Blot
 * @brief File containing defines for the project
 * @date 11 Fev 2020
 *
 * This header file contains defines for the project
 */


/*! \def Symbole
   \brief \c Symbole is a data structure containing a character and his occurence in a file
*/

#ifndef DEFINES_H_INCLUDED
#define DEFINES_H_INCLUDED

typedef struct {
  char character;
  int occurence;
  float proba;
  int totalNumber;
}charProba;



typedef struct  {
  charProba item[128];
  int number;
}t_queue;

void init(t_queue *q);
void enqueue(t_queue *q, charProba item);
int dequeue(t_queue *q);
int isEmpty(t_queue *q);



void FileStat(FILE *fich, charProba * SymboleArray);
void TriStat(charProba *SymboleArray) ;
void DecStat(charProba *SymboleArray) ;
void resultProba(charProba *charProbaArray, char *inputFilePath);
void initProba(charProba *SymboleArray);
int decryptLineSpec(char *line, int spec);
int CountFileCharacters(FILE *fich);

#endif // DEFINES_H_INCLUDED
