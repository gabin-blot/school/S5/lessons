/**
 * @file main.c
 * @author Gabin Blot
 * @brief File containing main for the TP
 * @date 11 février 2020
 *
 * This file contains main function for TP5
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "defines.h"
#include "string.h"

int main(int argc, char const *argv[]) {

  charProba charProbaArray[26];



  int verboseMode = 0;
  for (int arg = 2; arg < argc; arg++) {
    if(*argv[arg] == '-') {
      if (    *(argv[arg]+1) == 'V'
          ||  *(argv[arg]+1) == 'v')
          verboseMode = 1;
    }
  }

  char inputFilePath[256];
  strcpy(inputFilePath, (char*) argv[1]);

  if(inputFilePath == NULL) {
    printf("Error ! No file was given as a parameter\n");
    return 1;
  }

  printf("---------------------------------------------------------\n");

  FILE *inputFile;

  inputFile=fopen(inputFilePath, "r");
  if(inputFile != NULL)
  {
    FileStat(inputFile, charProbaArray);

    TriStat(charProbaArray);
  }
  else
    printf("Error while opening the file\n");

  fclose(inputFile);


  if(verboseMode) {
    for (int i = 0; i < 4; i++) {
      printf("charProba %d:%c, Occurence: %d Proba : %f\n", i, charProbaArray[i].character, charProbaArray[i].occurence, charProbaArray[i].proba);
    }
    printf("---------------------------------------------------------\n");
      DecStat(charProbaArray);


      printf("---------------------------------------------------------\n");
      printf("%s\n", inputFilePath );
      resultProba(charProbaArray, inputFilePath);
  }
  else {
    printf("Most probable candidate for E : %c so spec is <%d>\n", charProbaArray[0].character, charProbaArray[0].character-'E');
  }

  printf("---------------------------------------------------------\n");

  return 0;
}

void FileStat(FILE *fich, charProba *charProbaArray) {
  long charNumber = 0;

  for (int i = 0; i < 26; i++)
    charProbaArray[i].occurence = 0;

  char character;
  character = fgetc(fich);
  while (character != EOF) {
    charNumber++;
    if (character >= 'a' && character<='z')
      charProbaArray[character - 'a'].occurence++;
    else if (character >= 'A' && character<='Z')
      charProbaArray[character - 'A'].occurence++;
    character = fgetc(fich);
  }


  for (int i = 0; i < 26; i++) {
    charProbaArray[i].character = 'A' + i;
    charProbaArray[i].totalNumber = charNumber;
    charProbaArray[i].proba = (float) (charProbaArray[i].occurence * 100) / charNumber;
  }

}

void TriStat(charProba *charProbaArray) {
  charProba tempcharProbaArray[26];
  charProba tempcharProba;

  tempcharProbaArray[0] = charProbaArray[0];

  for (int parser = 0; parser < 25; parser++) {
    for (int i = 0; i < 25; i++) {
        if(charProbaArray[i].occurence < charProbaArray[i+1].occurence) {
          tempcharProba = charProbaArray[i];
          charProbaArray[i] = charProbaArray[i+1];
          charProbaArray[i+1] = tempcharProba;
        }
    }
  }


}

void DecStat(charProba *charProbaArray) {
  for (int i = 0; i < 4; i++) {
    printf("charProba %d: %c décalage: e=%d, \ts=%d, \ta=%d, \tn=%d\n", i, charProbaArray[i].character, (int) fabs(charProbaArray[i].character-'E'), (int) fabs(charProbaArray[i].character-'I'), (int) fabs(charProbaArray[i].character-'A'), (int) fabs(charProbaArray[i].character-'T'));
  }
}

void resultProba( charProba *charProbaArray, char *inputFilePath) {

  static char inputFilePathLocal[256];

  strcpy(inputFilePathLocal, inputFilePath);

  charProba stepProba[26];

  initProba(stepProba);

  for (int i = 0; i < 26; i++) {
    stepProba[i].character = i;
  }

  for (int i = 0; i < 3; i++) {
    stepProba[charProbaArray[i].character-'A'].occurence++;
    stepProba[charProbaArray[i].character-'E'].occurence++;
    stepProba[charProbaArray[i].character-'I'].occurence++;
    stepProba[charProbaArray[i].character-'T'].occurence++;
  }

  TriStat(stepProba);

  char * testString;
  int charNumber;


  FILE *inputFile;
  inputFile=fopen(inputFilePathLocal, "r");


  if(inputFile == NULL)
    printf("Error while opening the file\n");
  else {
    char character;
    int parser = 0;
    character = fgetc(inputFile);
    while (character != '\n') {
      parser++;
      character = fgetc(inputFile);
    }
    charNumber = parser;
  }
  fclose(inputFile);

  testString = malloc(sizeof(char) * charNumber);


  inputFile=fopen(inputFilePathLocal, "r");
  if(inputFile == NULL)
    printf("Error while opening the file\n");
  else {
    char character;
    int parser = 0;
    character = fgetc(inputFile);
    while (character != '\n') {
      testString[parser] = character;
      character = fgetc(inputFile);
      parser++;
    }
    testString[parser] = '\0';
  }
    fclose(inputFile);




  int i = 0;
  char testStringDecrypted[charNumber];
  do {
    strcpy(testStringDecrypted, testString);
    decryptLineSpec(testStringDecrypted, (int) stepProba[i].character);
    printf("Décalage de %d*%d \t: %s\n", stepProba[i].character, stepProba[i].occurence, testStringDecrypted);
    i++;
  } while(stepProba[i].occurence > 0 && i < 4);

  free(testString);



}

void initProba(charProba *SymboleArray) {
  for (int i = 0; i < 26; i++) {
    SymboleArray[i].character = 0;
    SymboleArray[i].occurence = 0;
    SymboleArray[i].proba = 0;
    SymboleArray[i].totalNumber = 0;
  }
}

int decryptLineSpec(char *line, int spec) {
  int i = 0;
  while (line[i] != '\0' && i <= 1000) {
    if(line[i] >= 'a' && line[i] <= 'z')
      if(line[i]-'a'-spec < 0)
        line[i] = (26 + (line[i]-'a'-spec))%26 + 'a';
      else
        line[i] = (line[i]-'a'-spec)%26 + 'a';
    if(line[i] >= 'A' && line[i] <= 'Z')
      if(line[i]-'A'-spec < 0)
        line[i] = (26 + (line[i]-'A'-spec))%26 + 'A';
      else
        line[i] = (line[i]-'A'-spec)%26 + 'A';
    i++;
  }
  return line[i]=='\0';
}


void init(t_queue *q) {
  //q -> head = q -> tail = 0;
}

void enqueue(t_queue *q, charProba item)
{
  q = realloc( q, q -> number * sizeof(charProba) );
}

int dequeue(t_queue *q)
{
  //q -> head %= 128;
  //return q -> item[(q -> head)++];
}

int isEmpty(t_queue *q)
{
    //  return 0;//(q -> tail) == (q -> head);
}
